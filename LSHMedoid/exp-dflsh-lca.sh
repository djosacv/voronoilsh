#!/bin/bash
echo "#experiments-distributionfree"

for nCluster in {10,30,50,80,100,200,500,1000,2000,3000,4000,5000};
do
	file="lshdistfree.jar.a0b"$nCluster"c0.log"
	echo $file
	echo "# exp-medoids nCluster="$nCluster" maxIter=0 sSize=0" > $file
	/usr/bin/time -f "#totaltime %Uuser %Ssystem %Eelapsed %PCPU (%Xtext+%Ddata %Mmax %Kavg)k" ./javajar-lca.sh lshdistfree.jar 0 $nCluster 0 &> $file
done
