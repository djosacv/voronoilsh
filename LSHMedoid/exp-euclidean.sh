#!/bin/bash
sampleSize=$1
echo "#experiments-euclidean with L="$1

maxIter=$2
for nCluster in {8,10,15,18,20,30,50,100,200};
do
	file="lsheuclidean.a"$sampleSize"b"$nCluster"c"$maxIter.log
	echo $file
	echo "# exp-euclidean K="$nCluster" W="$maxIter" L="$sampleSize > $file
	/usr/bin/time -f "#totaltime %Uuser %Ssystem %Eelapsed %PCPU (%Xtext+%Ddata %Mmax %Kavg)k" ./javajar.sh lsheuclidean.jar $sampleSize $nCluster $maxIter &> $file
done
