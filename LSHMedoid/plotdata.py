#! /usr/bin/env python
import sys
#selects the name of two columns based on the name in the first line of a column organized table

fp = sys.stdin


xl = int(sys.argv[1])
yl = int(sys.argv[2])

firstline=fp.readline()
firstline=firstline.split()


#plotfile = open("plotscript-"+sys.argv[1]+".plt","w+")
filestr="set grid;\nset xl '"+firstline[xl-1]+"';\nset yl '"+firstline[yl-1]+"';\nset key left top;\nplot 'data.out'  using 1:2 title '"+firstline[xl-1]+" X "+firstline[yl-1]+"' with linespoint;\n set terminal png;\n set output 'plot.png'; \n replot;\n set term post enh; \n set out 'plot.eps'; \n replot;\n"

sys.stdout.write(filestr)
#plotfile.write(filestr)
#plotfile.close()
#print("plotscript-"+sys.argv[1]+".plt")


