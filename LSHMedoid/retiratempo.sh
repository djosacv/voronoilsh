#!/bin/bash

for fp in $1/*.err;
do
	echo $fp 
	echo ${fp:0:-3}out
	grep "#time# ended  querying data" < $fp | cut -d":" -f2
done

