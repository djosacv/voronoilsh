#! /usr/bin/env python
import sys
#selects the name of two columns based on the name in the first line of a column organized table

fp = sys.stdin


xl = int(sys.argv[1])
yl = int(sys.argv[2])
zl = int(sys.argv[3])

firstline=fp.readline()
firstline=firstline.split()


#plotfile = open("plotscript-"+sys.argv[1]+".plt","w+")
filestr="set grid; set xl '"+firstline[xl-1]+"';\nset yl '"+firstline[yl-1]+"';\nset zl '"+firstline[zl-1]+"';set key top;\nset pm3d;\n set palette;\n set hidden3d back offset 1 trianglepattern 3 undefined 1 altdiagonal bentover;\n set style data lines;\n set ticslevel 0;\n splot 'data3d.out';\n set terminal png;\n set output 'plot3d.png'; \n replot;\n set term post enh; \n set out 'plot3d.eps'; \n replot;\n"

sys.stdout.write(filestr)
#plotfile.write(filestr)
#plotfile.close()
#print("plotscript-"+sys.argv[1]+".plt")


