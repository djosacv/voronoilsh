#!/bin/bash
if [ "$2" == "cluster" ] ; then
	echo "samples ncluster maxiter time queries_grounded queries_answered correct_fraction_avg correct_fraction_sdv correct_fraction_max";
fi

if [ "$2" == "e2" ] ; then
	echo "L K W time queries_grounded queries_answered correct_fraction_avg correct_fraction_sdv correct_fraction_max";
fi


for fp in $1/*.out;
do
	echo $fp | cut -d"." -f3- | cut -d"." -f1 | cut -d"a" -f2 | cut -d"b" -f1 | tr "\n" " " > temp
	echo $fp | cut -d"." -f3- | cut -d"." -f1 | cut -d"b" -f2 | cut -d"c" -f1 | tr "\n" " " >> temp
	echo $fp | cut -d"." -f3- | cut -d"." -f1 | cut -d"c" -f2 | tr "\n" " " >> temp
	grep "#time# ended  querying data" < ${fp:0:-3}err | cut -d":" -f2 | cut -d"m" -f1 | tr "\n" " " >> temp
	python ~/Dropbox/repo/evallejr/evalluateknn/evaluateknn.py ~/Dropbox/repo/my-lsh/LSHMedoid/apm100.truth500.match $fp  | cut -d":" -f2- | tr "\n" " " |tr "\t" " " >> temp
	cat temp
	echo "" 
	rm temp
done

