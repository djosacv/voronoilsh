#! /usr/bin/env python
import sys
# selects three columns in a data-file table (column order starts with 1).

c1 = int(sys.argv[1])
c2 = int(sys.argv[2])
c3 = int(sys.argv[3])
finalorder = []
for line in sys.stdin:
	sys.stdout.write(line)
	if("#" in line):
		elem=line.split()
		if(len(elem)>2):
			header = "#"+elem[c1-1]+" "+elem[c2-1]+" "+elem[c3-1]+"\n"
	else:
		elem = map(float,line.split())
		finalorder.append(tuple([elem[c1-1],elem[c2-1],elem[c3-1]]))
		
finalorder.sort()

dataout= open("data3d.out","w+")
dataout.write(header)
xatual=finalorder[0][0];
for i in finalorder:
	if(i[0]!=xatual):
		dataout.write("\n")
		xatual=i[0]
	dataout.write(str(i[0])+" "+str(i[1])+" "+str(i[2])+"\n");
dataout.close()

