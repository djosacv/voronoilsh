#!/bin/bash
nCluster=$1
echo "#experiments-medoids with nCluster="$nCluster

for maxIter in {1,2,3,5,7,9,10,15,20};
do
	for sampleSize in {5000,7000,10000,13000,15000,17000,20000,30000,50000,100000};
	do
		file="lshmedoid-itr.jar.a"$sampleSize"b"$nCluster"c"$maxIter".log"
		echo $file
		echo "# exp-medoids nCluster="$nCluster" maxIter="$maxIter" sSize="$sampleSize > $file
		/usr/bin/time -f "#totaltime %Uuser %Ssystem %Eelapsed %PCPU (%Xtext+%Ddata %Mmax %Kavg)k" ./javajar-lca.sh lshmedoid-itr.jar $sampleSize $nCluster $maxIter &> $file
	done
done
