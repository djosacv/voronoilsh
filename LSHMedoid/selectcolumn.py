#! /usr/bin/env python
import sys
# selects two columns in a data-file table (column order starts with 1).

c1 = int(sys.argv[1])
c2 = int(sys.argv[2])
finalorder = []
for line in sys.stdin:
	sys.stdout.write(line)
	if("#" in line):
		elem=line.split()
		if(len(elem)>2):
			header = "#"+elem[c1-1]+" "+elem[c2-1]+"\n"
	else:
		elem = map(float,line.split())
		sys.stderr.write(str(elem)+str(c1-1)+" "+str(c2-1)+"\n")
		finalorder.append(tuple([elem[c1-1],elem[c2-1]]))
		
finalorder.sort()

dataout= open("data.out","w+")
dataout.write(header)
for i in finalorder:
	dataout.write(str(i[0])+" "+str(i[1])+"\n");
dataout.close()

