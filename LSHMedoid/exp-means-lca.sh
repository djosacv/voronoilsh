#!/bin/bash
sampleSize=$1
echo "#experiments-medoids with sampleSize="$sampleSize

maxIter=$2
for nCluster in {10,30,50,80,100,200,500,1000,2000,3000,4000,5000};
do
	file="lshmeans.jar.a"$sampleSize"b"$nCluster"c"$maxIter".log"
	echo $file
	echo "# exp-means nCluster="$nCluster" maxIter="$maxIter" sSize="$sampleSize > $file
	/usr/bin/time -f "#totaltime %Uuser %Ssystem %Eelapsed %PCPU (%Xtext+%Ddata %Mmax %Kavg)k" ./javajar-lca.sh lshmeans.jar $sampleSize $nCluster $maxIter &> $file
done
