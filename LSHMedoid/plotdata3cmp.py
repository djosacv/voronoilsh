#! /usr/bin/env python
import sys
#selects the name of two columns based on the name in the first line of a column organized table

fp = sys.stdin


xl = int(sys.argv[1])
yl = int(sys.argv[2])

firstline=fp.readline()
firstline=firstline.split()


#plotfile = open("plotscript-"+sys.argv[1]+".plt","w+")
filestr="set grid;\n set logscale x;\n set xl '"+firstline[xl-1]+"';\nset yl '"+firstline[yl-1]+"';\nset key left top;\nset style line 1 lc rgb '#0060ad' lt 1 lw 5 pt 7 ps 1.5;\n set style line 2 lc rgb '#dd181f' lt 1 lw 5 pt 5 ps 1.5;\n set key left top;\nset style line 3 lc rgb '#FF00FF' lt 1 lw 5 pt 3 ps 1.5;\n  plot 'data.out'  using 1:2 title '"+sys.argv[3]+"' with linespoint ls 1;\n "
filestr=filestr+"rep 'data2.out'  using 1:2 title '"+sys.argv[4]+"' with linespoint ls 2;\n"
filestr=filestr+"rep 'data3.out'  using 1:2 title '"+sys.argv[5]+"' with linespoint ls 3;\n"

filestr=filestr+"set terminal png;\n set output 'plot.png'; \n replot;\n set term post enh color; \n set out 'plot.eps'; \n replot;\n"

sys.stdout.write(filestr)
#plotfile.write(filestr)
#plotfile.close()
#print("plotscript-"+sys.argv[1]+".plt")


