#!/bin/bash
sampleSize=$1
echo "#experiments-means with sampleSize="$1

maxIter=$2
for nCluster in {10,20,50,100,200,500,1000};
do
	file="lshmeans.a"$sampleSize"b"$nCluster"c"$maxIter.log
	echo $file
	echo "# exp-means nCluster="$nCluster" maxIter="$maxIter" sSize="$sampleSize > $file
	/usr/bin/time -f "#totaltime %Uuser %Ssystem %Eelapsed %PCPU (%Xtext+%Ddata %Mmax %Kavg)k" ./javajar.sh lshmeans.jar $sampleSize $nCluster $maxIter &> $file
done
