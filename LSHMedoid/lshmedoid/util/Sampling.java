package lshmedoid.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.math3.stat.clustering.EuclideanDoublePoint;

public class Sampling {

	public Sampling() {
		// TODO Auto-generated constructor stub
	}
	/*
	 * random sample m items of a collection
	 * 
	 */
	public static <T> ArrayList<T> randomSample(List<T> itms, int m)	{
	    Random rnd = new Random();
	    ArrayList<T> items = new ArrayList<T>(itms);
	    for(int i=0;i<m;i++){
	        int pos = i + rnd.nextInt(items.size() - i);
	        T tmp = items.get(pos);
	        items.set(pos, items.get(i));
	        items.set(i, tmp);
	    }
	    return new ArrayList<T>(items.subList(0, m));
	}
	public static ArrayList<Integer> randomIndex(int n, int m)	{
	    Random rnd = new Random();
	    ArrayList<Integer> items = new ArrayList<Integer>(n);
	    for(int i=0;i<n;i++){
	    	items.add(new Integer(i));
	    }
	    for(int i=0;i<m;i++){
	        int pos = i + rnd.nextInt(items.size() - i);
	        Integer tmp = items.get(pos);
	        items.set(pos, items.get(i));
	        items.set(i, tmp);
	    }
	    return new ArrayList<Integer>(items.subList(0, m));
	}
}
