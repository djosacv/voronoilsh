/**
 * 
 */
package lshmedoid.cluster;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import lshmedoid.test.LSHTest;
import lshmedoid.util.Sampling;




import org.apache.commons.math3.stat.clustering.Clusterable;
import org.apache.commons.math3.util.Pair;
/**
 * @author eliezer
 *
 */
public abstract class ClusteringAlgorithm <T extends Clusterable<T>> {
	public ArrayList<Cluster<T>> clusters = null;
	public static long countDists;
	public static double error;
	public int maxIter = 1000;
	
	@SuppressWarnings("static-access")
	public ClusteringAlgorithm(){
		this.countDists=0;
	}
	@SuppressWarnings("static-access")
	public ClusteringAlgorithm(int numiter){
		this.countDists=0;
		this.maxIter=numiter;
	}
	public int[] cluster(final ArrayList<T> points, final int k){
		return cluster(points,k,this.maxIter);
	}
	public static <T extends Clusterable<T>> float[][] distanceMatrix(final ArrayList<T> points){
		float[][] mat=new float[points.size()][points.size()];
		
		int i=0;
		for(T pi:points){
			int j=0;
			for(T pj:points){
				if(i<j)
					mat[i][j]=(int)pi.distanceFrom(pj);
				
				if(i==j)
					mat[i][j]=0;
				
				if(i>j)
					mat[i][j]=mat[j][i];
				
				j++;
			}
			i++;
		}
		
		
		return mat;
	}
	public int[] distmatrixCluster(final ArrayList<T> points, final int k,final int maxIterations){
		return cluster(points, k,maxIterations,distanceMatrix(points));
	}
	public abstract int[] cluster(final ArrayList<T> points, final int k, 
			final int maxIterations, float[][] dist);

	public abstract int[] cluster(final ArrayList<T> points, final int k, 
			final int maxIterations);
	
	public abstract int[] cluster(final ArrayList<T> points, final int k, 
			final int maxIterations,ArrayList<Cluster<T>> cclusters);

	/***
	 * given a set of points, a collection of datapoints and a vetor of assignments for each datapoint (add point[i] to cluster[j], if and only if assignment[i]=j)
	 * @param points
	 * @param clusters
	 * @param assignments
	 * @throws Exception 
	 */
	public static <T extends Clusterable<T>>  void addPointsToClusters (final ArrayList<T> points,ArrayList<Cluster<T>> clusters, int[] assignments) throws Exception {
		if(points.size()==assignments.length){
			if(clusters.isEmpty())
				throw new Exception("empty cluster list");
			for(int i=0;i<assignments.length;i++){
				clusters.get(assignments[i]).addPoint(i);
			}
		}else
			throw new Exception("different sizes of dataset and assignment vector. they should have the same size");
	}
	
	
	/**
	 * copied from org.apache.commons.math3.stat.clustering.KMeansPlusPlusClusterer
     * Adds the given points to the closest {@link Cluster}.
     *
     * @param <T> type of the points to cluster
     * @param clusters the {@link Cluster}s to add the points to
     * @param points the points to add to the given {@link Cluster}s
     * @param assignments points assignments to clusters
     * @return the number of points assigned to different clusters as the iteration before
     */
    public static <T extends Clusterable<T>> int
        assignPointsToClusters(final List<Cluster<T>> clusters, final Collection<T> points,
                               final int[] assignments) {
        int assignedDifferently = 0;
        int pointIndex = 0;
        for(Cluster<T> c:clusters){
        	c.getPoints().clear();
        }
        for (final T p : points) {
            int clusterIndex = getNearestCluster(clusters, p);
            if (clusterIndex != assignments[pointIndex]) {
                assignedDifferently++;
            }

            Cluster<T> cluster = clusters.get(clusterIndex);
            cluster.addPoint(pointIndex);
            assignments[pointIndex++] = clusterIndex;
        }

        return assignedDifferently;
    }
    
    
    /**
	 * copied from org.apache.commons.math3.stat.clustering.KMeansPlusPlusClusterer
     * Adds the given points to the closest {@link Cluster} using a distance matrix
     *
     * @param <T> type of the points to cluster
     * @param clusters the {@link Cluster}s to add the points to
     * @param points the points to add to the given {@link Cluster}s
     * @param assignments points assignments to clusters
     * @return the number of points assigned to different clusters as the iteration before
     */
    public static <T extends Clusterable<T>> int
        assignPointsToClustersDistMat(final List<Cluster<T>> clusters, final ArrayList<T> points,
                               final int[] assignments,float[][] dist) {
        int assignedDifferently = 0;
        int pointIndex = 0;
        for(Cluster<T> c:clusters){
        	c.getPoints().clear();
        }
        for (int i=0;i<points.size();i++) {
        	T p = points.get(i);
            int clusterIndex = getNearestClusterDistMat(clusters, i, dist);
            if (clusterIndex != assignments[pointIndex]) {
                assignedDifferently++;
            }

            Cluster<T> cluster = clusters.get(clusterIndex);
            cluster.addPoint(pointIndex);
            assignments[pointIndex++] = clusterIndex;
        }

        return assignedDifferently;
    }
    
    /**
	 * Random initialization method of clustering algorithm. Select k random points from pointset ArrayList<T> points and return an ArrayList<Cluster<T>> of k clusters
	 * @param points
	 * @param k
	 * @return
	 */
    public static <T extends Clusterable<T>>  ArrayList<Cluster<T>> randomInit(final ArrayList<T> points, final int k){
		ArrayList<T> pointsclusters=new ArrayList<T>( Sampling.randomSample(points,k));
		ArrayList<Cluster<T>> cclusters = new ArrayList<Cluster<T>>(k);
		for(int i=0;i<k;i++){
			cclusters.add( new Cluster<T>(pointsclusters.get(i),i));
		}
		return cclusters;
	}
    
    
    /**
	 * initialization method of clustering algorithm taken from "A Simple and fast algorithm for K-medoids clustering" by Park and Jun [Expert Systems with Applications] 
	 * Select k central points from pointset ArrayList<T> points and return an ArrayList<Cluster<T>> of k clusters.
	 * @param points
	 * @param k
	 * @param dist - distance matrix
	 * @return
	 */
    public static <T extends Clusterable<T>>  ArrayList<Cluster<T>> fastKMedoidInitdistMat(final ArrayList<T> points, final int k,float[][] dist){
		ArrayList<T> pointsclusters=new ArrayList<T>(k);
		int n = points.size();
		
		
		
		ArrayList<PairDoubleClusterIndex> indexdist = new ArrayList<PairDoubleClusterIndex>(n);
		for(int i=0;i<n;i++){
			indexdist.add(new PairDoubleClusterIndex(0.0,i));
		}
		for(int i=0;i<n;i++){
			double si=0.0;
			for(int l=0;l<n;l++){
				si+=dist[i][l];
			}
			for(int j=0;j<n;j++){
				PairDoubleClusterIndex vj=indexdist.get(j);
				
				vj.dist+=dist[j][i]/si;
			}
		}
		Collections.sort(indexdist);
		
		
		
		
		ArrayList<Cluster<T>> cclusters = new ArrayList<Cluster<T>>(k);
		for(int i=0;i<k;i++){
			cclusters.add( new Cluster<T>(pointsclusters.get(i),indexdist.get(i).index));
		}
		
		return cclusters;
	}
    
    /**
   	 * initialization method of clustering algorithm taken from "A Simple and fast algorithm for K-medoids clustering" by Park and Jun [Expert Systems with Applications] 
   	 * Select k central points from pointset ArrayList<T> points and return an ArrayList<Cluster<T>> of k clusters.
   	 * @param points
   	 * @param k
   	 * @return
   	 */
       public static <T extends Clusterable<T>>  ArrayList<Cluster<T>> fastKMedoidInit(final ArrayList<T> points, final int k){
   		int n = points.size();
   		
   		
   		
   		ArrayList<PairDoubleClusterIndex> indexdist = new ArrayList<PairDoubleClusterIndex>(n);
   		for(int i=0;i<n;i++){
   			indexdist.add(new PairDoubleClusterIndex(0.0,i));
   		}
   		for(int i=0;i<n;i++){
   			double si=0.0;
   			T pi=points.get(i);
   			for(int l=0;l<n;l++){
   				si+=pi.distanceFrom(points.get(l));
   			}
   			for(int j=0;j<n;j++){
   				PairDoubleClusterIndex vj=indexdist.get(j);
   				
   				vj.dist+=pi.distanceFrom(points.get(j))/si;
   			}
   		}
   		Collections.sort(indexdist);
   		
   		
   		
   		
   		ArrayList<Cluster<T>> cclusters = new ArrayList<Cluster<T>>(k);
   		for(int i=0;i<k;i++){
   			int cind=indexdist.get(i).index;
   			cclusters.add( new Cluster<T>(points.get(cind)));
   		}
   		
   		return cclusters;
   	}
    
    /**
     * Use K-means++ to choose the initial centers.
     * copied from org.apache.commons.math3.stat.clustering.KMeansPlusPlusClusterer
     * @param <T> type of the points to cluster
     * @param points the points to choose the initial centers from
     * @param k the number of centers to choose
     * @param random random generator to use
     * @return the initial centers
     */
    public static <T extends Clusterable<T>> ArrayList<Cluster<T>>
        chooseInitialCentersKMeansPP(final ArrayList<T> pointList, final int k) {
    	Random random = new Random();

        // Convert to list for indexed access. Make it unmodifiable, since removal of items
        // would screw up the logic of this method.
        //final List<T> pointList = Collections.unmodifiableList(new ArrayList<T> (points));

        // The number of points in the list.
        final int numPoints = pointList.size();

        // Set the corresponding element in this array to indicate when
        // elements of pointList are no longer available.
        final boolean[] taken = new boolean[numPoints];

        // The resulting list of initial centers.
        final ArrayList<Cluster<T>> resultSet = new ArrayList<Cluster<T>>();

        // Choose one center uniformly at random from among the data points.
        final int firstPointIndex = random.nextInt(numPoints);

        final T firstPoint = pointList.get(firstPointIndex);

        resultSet.add(new Cluster<T>(firstPoint));

        // Must mark it as taken
        taken[firstPointIndex] = true;

        // To keep track of the minimum distance squared of elements of
        // pointList to elements of resultSet.
        final double[] minDistSquared = new double[numPoints];

        // Initialize the elements.  Since the only point in resultSet is firstPoint,
        // this is very easy.
        for (int i = 0; i < numPoints; i++) {
            if (i != firstPointIndex) { // That point isn't considered
                double d = firstPoint.distanceFrom(pointList.get(i));
                minDistSquared[i] = d*d;
            }
        }

        while (resultSet.size() < k) {

            // Sum up the squared distances for the points in pointList not
            // already taken.
            double distSqSum = 0.0;

            for (int i = 0; i < numPoints; i++) {
                if (!taken[i]) {
                    distSqSum += minDistSquared[i];
                }
            }

            // Add one new data point as a center. Each point x is chosen with
            // probability proportional to D(x)2
            final double r = random.nextDouble() * distSqSum;

            // The index of the next point to be added to the resultSet.
            int nextPointIndex = -1;

            // Sum through the squared min distances again, stopping when
            // sum >= r.
            double sum = 0.0;
            for (int i = 0; i < numPoints; i++) {
                if (!taken[i]) {
                    sum += minDistSquared[i];
                    if (sum >= r) {
                        nextPointIndex = i;
                        break;
                    }
                }
            }

            // If it's not set to >= 0, the point wasn't found in the previous
            // for loop, probably because distances are extremely small.  Just pick
            // the last available point.
            if (nextPointIndex == -1) {
                for (int i = numPoints - 1; i >= 0; i--) {
                    if (!taken[i]) {
                        nextPointIndex = i;
                        break;
                    }
                }
            }

            // We found one.
            if (nextPointIndex >= 0) {

                final T p = pointList.get(nextPointIndex);

                resultSet.add(new Cluster<T> (p));

                // Mark it as taken.
                taken[nextPointIndex] = true;

                if (resultSet.size() < k) {
                    // Now update elements of minDistSquared.  We only have to compute
                    // the distance to the new center to do this.
                    for (int j = 0; j < numPoints; j++) {
                        // Only have to worry about the points still not taken.
                        if (!taken[j]) {
                            double d = p.distanceFrom(pointList.get(j));
                            double d2 = d * d;
                            if (d2 < minDistSquared[j]) {
                                minDistSquared[j] = d2;
                            }
                        }
                    }
                }

            } else {
                // None found --
                // Break from the while loop to prevent
                // an infinite loop.
                break;
            }
        }

        return resultSet;
    }
    
    /**
     * copied from org.apache.commons.math3.stat.clustering.KMeansPlusPlusClusterer
     * Returns the nearest {@link Cluster} to the given point
     *
     * @param <T> type of the points to cluster
     * @param clusters the {@link Cluster}s to search
     * @param point the point to find the nearest {@link Cluster} for
     * @return the index of the nearest {@link Cluster} to the given point
     */
    public static <T extends Clusterable<T>> int
        getNearestCluster(final Collection<Cluster<T>> clusters, final T point) {
        double minDistance = Double.MAX_VALUE;
        int clusterIndex = 0;
        int minCluster = 0;
        for (final Cluster<T> c : clusters) {
            final double distance = point.distanceFrom(c.getCenter());
            countDists++;
            if (distance < minDistance) {
                minDistance = distance;
                minCluster = clusterIndex;
            }
            clusterIndex++;
        }

        return minCluster;
    }
    
    /**
     * copied from org.apache.commons.math3.stat.clustering.KMeansPlusPlusClusterer
     * Returns the nearest {@link Cluster} to the given point
     *
     * @param <T> type of the points to cluster
     * @param clusters the {@link Cluster}s to search
     * @param point the point to find the nearest {@link Cluster} for
     * @return the index of the nearest {@link Cluster} to the given point
     */
    public static <T extends Clusterable<T>> Pair<Integer,Double>
        getNearestClusterDistance(final Collection<Cluster<T>> clusters, final T point) {
        double minDistance = Double.MAX_VALUE;
        int clusterIndex = 0;
        int minCluster = 0;
        for (final Cluster<T> c : clusters) {
            final double distance = point.distanceFrom(c.getCenter());
            countDists++;
            if (distance < minDistance) {
                minDistance = distance;
                minCluster = clusterIndex;
            }
            clusterIndex++;
        }
        
        return new Pair<Integer,Double>(new Integer(minCluster),new Double(minDistance));
    }
    
    
    
    
    /**
     * copied from org.apache.commons.math3.stat.clustering.KMeansPlusPlusClusterer
     * Returns the nearest {@link Cluster} to the given point
     *
     * @param <T> type of the points to cluster
     * @param clusters the {@link Cluster}s to search
     * @param point the point to find the nearest {@link Cluster} for
     * @return the index of the nearest {@link Cluster} to the given point
     */
    public static <T extends Clusterable<T>> int
        getNearestClusterDistMat(final Collection<Cluster<T>> clusters, int pointIndex, float[][] dist) {
        double minDistance = Double.MAX_VALUE;
        int clusterIndex = 0;
        int minCluster = 0;
        for (final Cluster<T> c : clusters) {
            final double distance = dist[pointIndex][c.getIndex()];
            countDists++;
            if (distance < minDistance) {
                minDistance = distance;
                minCluster = clusterIndex;
            }
            clusterIndex++;
        }

        return minCluster;
    }
    
    
    public static void countdist(){
    	countDists++;
    }
    public static long getCountdist(){
    	return countDists;
    }
    
    public static void resetCounter(){
    	countDists=0;
    }
    
	/**
	 * @return the clusters
	 */
	public ArrayList<Cluster<T>> getClusters() {
		return clusters;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String str="Clusters: \n";
		for(int i=0;i<clusters.size();i++) {
			str+="\t k="+i+" size="+clusters.get(i).getPoints().size()+": "+clusters.get(i).getCenter()+"\n";
		}
		
		return str;
	}
	public String toStringInline(ArrayList<Cluster<T>> clusters  ) {
		String str="Clusters: \n";
		for(int i=0;i<clusters.size();i++) {
			str+="\t k="+i+" size="+clusters.get(i).getPoints().size()+": "+clusters.get(i).getCenter();
		}
		
		return str;
	}
	/**
	 * @param clusters the clusters to set
	 */
	public void setClusters(ArrayList<Cluster<T>> clusters) {
		this.clusters = clusters;
	}
	

	static class PairDoubleClusterIndex implements Comparable<PairDoubleClusterIndex>{
		public double dist;
		public int index;

		public PairDoubleClusterIndex(double d,int index){
			this.dist=d;
			this.index=index;
		}

		@Override
		public int compareTo(PairDoubleClusterIndex o) {
			return Double.compare(dist, o.dist);
		}
	}
}
