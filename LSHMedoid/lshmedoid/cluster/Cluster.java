/**
 * 
 */
package lshmedoid.cluster;


/**
 * @author eliezer
 *
 */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.math3.stat.clustering.Clusterable;
/**
 * Adapted from org.apache.commons.math3.stat.clustering.Cluste; (added a index)
 * Cluster holding a set of {@link Clusterable} points.
 * @param <T> the type of points that can be clustered
 * @version $Id: Cluster.java 1416643 2013-02-07 tn $
 * @since 2.1
 */
public class Cluster<T extends Clusterable<T>> implements Serializable {

    /** Serializable version identifier. */
    private static final long serialVersionUID = -3442297081515880464L;

    /** The points contained in this cluster. */
    private List<Integer> points; //list of indexes in a given point datastructure.

    /** Center of the cluster. */
    private T center;
    
    /** cluster index in a array of points **/
    private int index;

    /**
     * Build a cluster centered at a specified point.
     * cluster index is not set
     * @param center the point which is to be the center of this cluster
     */
    public Cluster(final T center) {
        this.center = center;
        points = new ArrayList<Integer>();
        this.index=-1;
    }
    /**
     * Build a cluster centered at a specified point of a collection of points
     * @param datapoints is the collection of points and index is the cluster index in the collection
     */
    public Cluster(T center,int indx) {
        this.center = center;
        points = new ArrayList<Integer>();
        this.index=indx;
    }
    public void update(T center,int indx) {
        this.center = center;
        this.index=indx;
        points.clear();
    }
    /**
     * Build a cluster centered at a specified point of a collection of points
     * @param datapoints is the collection of points and index is the cluster index in the collection
     */
    public Cluster(ArrayList<T> datapoints,int indx) {
        this.center = datapoints.get(indx);
        points = new ArrayList<Integer>();
        this.index=indx;
    }

    /**
     * Add a point to this cluster.
     * @param point point to add
     */
    public void addPoint(final Integer point) {
        points.add(point);
    }
    
    /**
     * Add a point to this cluster.
     * @param point point to add
     */
    public void addPoint(final int point) {
        points.add(new Integer(point));
    }

    /**
     * Get the points contained in the cluster.
     * @return points contained in the cluster
     */
    public List<Integer> getPoints() {
        return points;
    }

    /**
     * Get the point chosen to be the center of this cluster.
     * @return chosen cluster center
     */
    public T getCenter() {
        return center;
    }
	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}
	/**
	 * @param index the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}
	
	public String toString(){
		return this.center.toString();
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.index;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Cluster<T> obj) {
		return obj.index==this.index;
	}
	
}
