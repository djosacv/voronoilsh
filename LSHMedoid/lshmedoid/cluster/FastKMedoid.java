/**
 * 
 */
package lshmedoid.cluster;


import java.util.ArrayList;
import java.util.List;
import lshmedoid.test.LSHTest;
import org.apache.commons.math3.stat.clustering.Clusterable;
/**
 * @author eliezer
 *
 */
public class FastKMedoid <T extends Clusterable<T>> extends ClusteringAlgorithm<T>  {

	@Override
	public int[] cluster(ArrayList<T> points, int k, int maxIterations,
			float[][] dist) {
		// TODO Auto-generated method stub
		ArrayList<Cluster<T>> cclusters = randomInit(points,k);
		return cluster(points,k,maxIterations,dist,cclusters);
	}
	public int[] cluster(final ArrayList<T> points, final int k, 
			final int maxIterations, float[][] dist,ArrayList<Cluster<T>> cclusters){

		
		int n = points.size();
		int[] assignments = new int[n];
		//
		
		
		assignPointsToClustersDistMat(cclusters,points,assignments,dist);
		//LSHTest.log(" --",toStringInline(cclusters));
		boolean change = true;
		long start = System.currentTimeMillis();
		// Begin the main loop of alternating optimization
		for (int itr = 0; itr < maxIterations && change; ++itr) {
			// assigning each object to the nearest medoid
			LSHTest.log(" kmedoid_iter",itr);
			start = System.currentTimeMillis();
			

			// Try to find a better set of medoids (local search)
			change = false;
			int newCluster;
			for (int l = 0; l < k; l++) {
				newCluster=updateCluster(cclusters.get(l), points);
				if(newCluster!=cclusters.get(l).getIndex())
				{
					cclusters.get(l).update(points.get(newCluster), newCluster);
					change = true;
				}
				
			}
			assignPointsToClusters(cclusters,points,assignments);
			
			LSHTest.logTime(" kmedoid_iter "+itr,start);
			//LSHTest.log(" --",toStringInline(cclusters));
		}
		this.clusters=cclusters;
		return assignments;	

    }
	/**
	 * clusters method with parametrized by the initial cluster centers.
	 * @param points
	 * @param k
	 * @param maxIterations
	 * @param cclusters - initial clusters extracted from data points
	 * @return
	 */
	public int[] cluster(final ArrayList<T> points, final int k, 
			final int maxIterations,ArrayList<Cluster<T>> cclusters){
		
		int n = points.size();
		int[] assignments = new int[n];
		//
		
		
		assignPointsToClusters(cclusters,points,assignments);
		//LSHTest.log(" --",toStringInline(cclusters));
		boolean change = true;
		long start = System.currentTimeMillis();
		// Begin the main loop of alternating optimization
		for (int itr = 0; itr < maxIterations && change; ++itr) {
			// assigning each object to the nearest medoid
			LSHTest.log(" kmedoid_iter",itr);
			start = System.currentTimeMillis();
			

			// Try to find a better set of medoids (local search)
			change = false;
			int newCluster;
			for (int l = 0; l < k; l++) {
				newCluster=updateCluster(cclusters.get(l), points);
				if(newCluster!=cclusters.get(l).getIndex())
				{
					cclusters.get(l).update(points.get(newCluster), newCluster);
					change = true;
				}
				
			}
			assignPointsToClusters(cclusters,points,assignments);
			
			LSHTest.logTime(" kmedoid_iter "+itr,start);
			//LSHTest.log(" --",toStringInline(cclusters));
		}
		this.clusters=cclusters;
		return assignments;	
    }
	
	/**
	 * standard clustering with random initialization of cluster centers
	 * @param points
	 * @param k
	 * @param maxIterations
	 * @return
	 */
	public int[] cluster(final ArrayList<T> points, final int k, 
			final int maxIterations){
				
		ArrayList<Cluster<T>> cclusters = randomInit(points,k);
		return cluster(points,k,maxIterations,cclusters);
		
    }
	
	
	
	/**
	 * performs a local search in the cluster for a point which minimizes
	 * @param cluster
	 * @param data
	 * @return
	 */
	private int updateCluster(Cluster<T> cluster, ArrayList<T> data) {
		int clusterIdx=cluster.getIndex();
		List<Integer> clustersPoints = cluster.getPoints();
		double oldcost=0.0;
		double newcost=0.0;
		
		for(Integer idx:clustersPoints){
			oldcost+=cluster.getCenter().distanceFrom(data.get(idx));
			ClusteringAlgorithm.countdist();
		}
		double lowestnew=oldcost;
		for(Integer newClusterIdx:clustersPoints){
			
			T candidateC = data.get(newClusterIdx); 
			newcost=0.0;
			for(Integer idx:clustersPoints){
				if(idx!=newClusterIdx){
					newcost+=candidateC.distanceFrom(data.get(idx));
					ClusteringAlgorithm.countdist();
				}
			}
			if(newcost < lowestnew){
				lowestnew = newcost;
				clusterIdx=newClusterIdx;
				
			}
		}
		
		
		return clusterIdx;
	}
	
	/**
	 * performs a local search in the cluster for a point which minimizes
	 * @param cluster
	 * @param data
	 * @return
	 */
	private int updateClusterDistMat(Cluster<T> cluster, ArrayList<T> data,float[][] dist) {
		int clusterIdx=cluster.getIndex();
		List<Integer> clustersPoints = cluster.getPoints();
		double oldcost=0.0;
		double newcost=0.0;
		
		for(Integer idx:clustersPoints){
			oldcost+=dist[cluster.getIndex()][idx];
			ClusteringAlgorithm.countdist();
		}
		double lowestnew=oldcost;
		for(Integer newClusterIdx:clustersPoints){
			

			newcost=0.0;
			for(Integer idx:clustersPoints){
				if(idx!=newClusterIdx){
					newcost+=dist[newClusterIdx][idx];
					ClusteringAlgorithm.countdist();
				}
			}
			if(newcost < lowestnew){
				lowestnew = newcost;
				clusterIdx=newClusterIdx;
				
			}
		}
		
		
		return clusterIdx;
	}
	
}
