/**
 * 
 */
package lshmedoid.cluster;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import lshmedoid.test.LSHTest;
import lshmedoid.util.Sampling;



import org.apache.commons.math3.stat.clustering.Clusterable;
/**
 * @author eliezer
 *
 */
public class RandomMedoid <T extends Clusterable<T>> extends ClusteringAlgorithm<T>  {

	
	public int[] cluster(final ArrayList<T> points, final int k, 
			final int maxIterations, float[][] dist){
		int[] assignments = new int[points.size()];
		/** TODO
		 *  - implement this variation
		 */
		return assignments;	
    }
	public int[] cluster(final ArrayList<T> points,int k){
		
		int n = points.size();
		int[] assignments = new int[n];
		//
		ArrayList<T> pointsclusters=new ArrayList<T>( Sampling.randomSample(points,k));
		ArrayList<Cluster<T>> cclusters = new ArrayList<Cluster<T>>(k);
		for(int i=0;i<k;i++){
			cclusters.add( new Cluster<T>(pointsclusters.get(i),i));
		}
		
		this.clusters=cclusters;
		return assignments;	
    }
	@Override
	public int[] cluster(ArrayList<T> points, int k, int maxIterations) {
		return cluster(points,k);
	}
	@Override
	public int[] cluster(ArrayList<T> points, int k, int maxIterations,
			ArrayList<Cluster<T>> cclusters) {
		// TODO Auto-generated method stub
		this.clusters=cclusters;
		return null;
	}
}
