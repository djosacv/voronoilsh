/**
 * 
 */
package lshmedoid.cluster;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import lshmedoid.test.LSHTest;
import lshmedoid.util.Sampling;



import org.apache.commons.math3.stat.clustering.Clusterable;
/**
 * @author eliezer
 *
 */
public class PAMKMedoid <T extends Clusterable<T>> extends ClusteringAlgorithm<T> {

	public int[] cluster(final ArrayList<T> points, final int k, 
			final int maxIterations, float[][] dist){
		int[] assignments = new int[points.size()];
		int n = points.size();
		//
		ArrayList<T> pointsclusters=new ArrayList<T>( Sampling.randomSample(points,k));
		ArrayList<Cluster<T>> cclusters = new ArrayList<Cluster<T>>(k);
		for(int i=0;i<k;i++){
			cclusters.add( new Cluster<T>(pointsclusters.get(i),i));
		}
		
		boolean change = true;
		long start = System.currentTimeMillis();
		// Begin the main loop of alternating optimization
		for (int itr = 0; itr < maxIterations && change; ++itr) {
			// assigning each object to the nearest medoid
			LSHTest.log(" kmedoid_iter",itr);
			start = System.currentTimeMillis();
			assignPointsToClusters(cclusters,points,assignments);

			// Try to find a better set of medoids
			change = false;
			// adapted from 
			for (int l = 0; l < k; l++) {

				// For each non-medoid in the cluster
				int medoid = cclusters.get(l).getIndex();
				///LSHTest.log(" medoid ",medoid);
				for (int i = 0; i < n; ++i) {
	               int bestMedoid = medoid;
	               //LSHTest.log(" bestMedoid ",bestMedoid);
	               double lowestCostDelta = 0;
	               if (i != medoid ) {
	                  // Calculate the change in cost by swapping this configuration
	                  int costDelta = 0;
	                  //LSHTest.log(" started distance loop",i);
	                  //long start2 = System.currentTimeMillis();
	                  for (int j = 0; j < n; ++j) {
	                     double oldDistance=dist[j][medoid];
	                     double newDistance = dist[j][i];
	                     costDelta += newDistance - oldDistance;
	                  }
	                  //LSHTest.logTime(" distance loop "+i+" ",start2);

	                  if (costDelta < lowestCostDelta) {
	                     bestMedoid = i;
	                     lowestCostDelta = costDelta;
	                  }
	                  //LSHTest.log(" bestMedoid2 ",bestMedoid);
	                  //LSHTest.log(" lowestCostDelta ",lowestCostDelta);
	                  if (bestMedoid != medoid) {
	                	 cclusters.set(l, new Cluster<T>(points,bestMedoid));
	                     change = true;
	                  }
	               }
				}
			}
			LSHTest.logTime(" kmedoid_iter "+itr,start);
		}
		this.clusters=cclusters;
		return assignments;	
    }
	public int[] cluster(final ArrayList<T> points, final int k, 
			final int maxIterations){
		int[] assignments = new int[points.size()];
		int n = points.size();
		//
		ArrayList<T> pointsclusters=new ArrayList<T>( Sampling.randomSample(points,k));
		ArrayList<Cluster<T>> cclusters = new ArrayList<Cluster<T>>(k);
		for(int i=0;i<k;i++){
			cclusters.add( new Cluster<T>(pointsclusters.get(i),i));
		}
		LSHTest.log(" --",toStringInline(cclusters));
		boolean change = true;
		long start = System.currentTimeMillis();
		// Begin the main loop of alternating optimization
		for (int itr = 0; itr < maxIterations && change; ++itr) {
			// assigning each object to the nearest medoid
			LSHTest.log(" kmedoid_iter",itr);
			start = System.currentTimeMillis();
			assignPointsToClusters(cclusters,points,assignments);

			// Try to find a better set of medoids
			change = false;
			// adapted from 
			for (int l = 0; l < k; l++) {

				// For each non-medoid in the cluster
				int medoid = cclusters.get(l).getIndex();
				///LSHTest.log(" medoid ",medoid);
				for (int i = 0; i < n; ++i) {
	               int bestMedoid = medoid;
	               //LSHTest.log(" bestMedoid ",bestMedoid);
	               double lowestCostDelta = 0;
	               if (i != medoid ) {
	                  // Calculate the change in cost by swapping this configuration
	                  int costDelta = 0;
	                  //LSHTest.log(" started distance loop",i);
	                  //long start2 = System.currentTimeMillis();
	                  for (int j = 0; j < n; ++j) {
	                     double oldDistance=points.get(j).distanceFrom(points.get(medoid));
	                     double newDistance = points.get(j).distanceFrom(points.get(i));
	                     costDelta += newDistance - oldDistance;
	                  }
	                  //LSHTest.logTime(" distance loop "+i+" ",start2);

	                  if (costDelta < lowestCostDelta) {
	                     bestMedoid = i;
	                     lowestCostDelta = costDelta;
	                  }
	                  //LSHTest.log(" bestMedoid2 ",bestMedoid);
	                  //LSHTest.log(" lowestCostDelta ",lowestCostDelta);
	                  if (bestMedoid != medoid) {
	                	 cclusters.set(l, new Cluster<T>(points.get(bestMedoid),bestMedoid));
	                     change = true;
	                  }
	               }
				}
			}
			LSHTest.logTime(" kmedoid_iter "+itr,start);
			LSHTest.log(" --",toStringInline(cclusters));
		}
		this.clusters=cclusters;
		return assignments;	
    }
	@Override
	public int[] cluster(ArrayList<T> points, int k, int maxIterations,
			ArrayList<Cluster<T>> cclusters) {
		// TODO Auto-generated method stub
		return null;
	}

}
