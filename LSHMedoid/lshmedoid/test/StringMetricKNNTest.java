package lshmedoid.test;

import lshmedoid.cluster.ClusteringAlgorithm;
import lshmedoid.data.StringFileReader;
import lshmedoid.data.StringLevenshteinPoint;
import lshmedoid.test.LSHTest.Experiment;


public class StringMetricKNNTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
			LSHTest<StringLevenshteinPoint> lshrunning=new LSHTest<StringLevenshteinPoint>(args[0],args[1],new StringFileReader());
			LSHTest.runExperiment(args,LSHTest.Experiment.DFLSH,lshrunning);

		}catch(IndexOutOfBoundsException e)
		{
			e.printStackTrace();
			System.err.println("You must provide dataset file path and query file path");
		}catch(Exception e1)
		{
			e1.printStackTrace();
		}
		
	}

}
