package lshmedoid.test;

import java.io.IOException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import lshmedoid.cluster.Cluster;
import lshmedoid.cluster.ClusteringAlgorithm;
import lshmedoid.cluster.FastKMedoid;
import lshmedoid.cluster.RandomMedoid;
import lshmedoid.data.APMIntegerPointsReader;
import lshmedoid.data.DataReader;
import lshmedoid.lsh.KLSHFunction;
import lshmedoid.lsh.KLSHResampleFunction;
import lshmedoid.lsh.LSH;
import lshmedoid.lsh.LSHFunction;
import lshmedoid.util.Sampling;

import org.apache.commons.math3.stat.clustering.*;

public class LSHTest<T extends Clusterable<T>> {
	String truthFileName;
    String testFileName;
    String outputFileName;
    String datasetFileName;
    String queryFileName;
    String dirName;
    static int kNN=30;
    ArrayList<Integer> idBaseKeyPoints;
    ArrayList<Integer> idQueryKeyPoints;
    ArrayList<T> data;
    ArrayList<T> queries;
    DataReader<T> datareader;
    public static boolean logStatistics=true;

    

	public LSHTest(
		    String datasetFileName,
		    String queryFileName
			) {
		this.datasetFileName=datasetFileName;
		this.queryFileName=queryFileName;
		this.idBaseKeyPoints=new ArrayList<Integer>();
		this.idQueryKeyPoints=new ArrayList<Integer>();
		this.data=null;
		this.queries=null;
		
	}
	public LSHTest(
		    String datasetFileName,
		    String queryFileName,
		    DataReader<T> datareader
			) {
		this.datasetFileName=datasetFileName;
		this.queryFileName=queryFileName;
		this.idBaseKeyPoints=new ArrayList<Integer>();
		this.idQueryKeyPoints=new ArrayList<Integer>();
		this.data=null;
		this.queries=null;
		this.datareader=datareader;
		
	}

    public void loadFiles() throws IOException{
    	if(data==null)
			data = datareader.readSignatures(this.datasetFileName,this.idBaseKeyPoints);
		else
			log("dataset already loaded");
			
		
		if(queries==null)
			queries = datareader.readSignatures(this.queryFileName,this.idQueryKeyPoints);
		else
			log("query set already loaded");
    }
    public void processAndQuery(LSH<T> lsh){
    	processAndQuery(lsh,0);
    }
    
    public void processAndQuery(LSH<T> lsh, int mode){
    	// 3) Process data using this KLSH function (basicly index all data in a hash table)
    	
		
    	log(" started indexing data");
		long start = System.currentTimeMillis();
		lsh.processData(data, mode);
		logTime(" indexing data",start);
		
		log("-lsh stats");
    	lsh.stats();
		// 4) Scan all queries and get 30-NN results
		int i=0;
		log(" started querying data");
		start = System.currentTimeMillis();
		
		System.out.print("MATCHPOINTS\n"+kNN+"\n");
		for(T query : queries){
			long startQuery = System.currentTimeMillis();
			LinkedList<Integer> result = lsh.query2(query,data, kNN,mode);
			// Integer:ID_of_query_keypoint { Integer:ID_of_database_keypoint Float:Distance ... }
			logTime(" query ",startQuery);
			System.out.print(this.idQueryKeyPoints.get(i));
			for(Integer idxbase:result){
				System.out.print(" "+this.idBaseKeyPoints.get(idxbase)+" "+data.get(idxbase).distanceFrom(query));
			}
			System.out.print("\n");
			
			i++;
		}
		logTime(" querying data ",start);
    }
    public void runExperimentKMedoid(
    		int sampleSize,
    		int numberOfClusters,
    		int maxIter,int L) 
    				throws IOException{
    	
     	// random init
    	// 0) Pre-process files in disk and load dataset, queries and ground truth in primary memory
    	loadFiles();
				
		
		// 1) Sample part of the data and running K-Medoid
				
		log(" Experiment setup");
		log(" DatasetSize",data.size());
		log(" sampleSize",sampleSize);
		log(" numberOfClusters",numberOfClusters);
		log(" maxIter",maxIter);
		log(" number-of-functions",L);
		long start = System.currentTimeMillis();
		ArrayList<LSHFunction<T>> lshfuns=new ArrayList<LSHFunction<T>>();
		for(int i=0;i<L;i++){
			start = System.currentTimeMillis();
			log(" Started sampling points");
			log("L-run",i);
			ArrayList<T> sample= Sampling.randomSample(data, sampleSize);
			logTime("  sampling "+sample.size()+" data poins ",start);
			
			log(" started k-medoid");
			start = System.currentTimeMillis();
			
			FastKMedoid<T> kmedoid = new FastKMedoid<T>();
			kmedoid.cluster(
					sample
					,numberOfClusters, maxIter);
			
	        logTime(" k-medoid ",start);
	        KLSHFunction<T> fun=new KLSHFunction<T>(kmedoid.getClusters());
	        fun.saveClustersFile("centroid-kmedoidRandomInitK"+numberOfClusters+"sample"+sampleSize+"L"+i);
	        lshfuns.add(fun);
		}

		// 2) Use cluster from previous step to create a KLSH function
        log(" created LSHMedoidFunction");
        start = System.currentTimeMillis();
		LSH<T> lsh=new LSH<T>(lshfuns);
		logTime(" creating LSHMedoidFunction",start);


		processAndQuery(lsh);

    }
    
    public void runExperimentKMedoidInit(
    		int sampleSize,
    		int numberOfClusters,
    		int maxIter,
    		int L) 
    				throws IOException{
    	
    	// random init
    	// 0) Pre-process files in disk and load dataset, queries and ground truth in primary memory
    	loadFiles();
				
		
		// 1) Sample part of the data and running K-Medoid
				
		log(" Experiment setup");
		log(" DatasetSize",data.size());
		log(" sampleSize",sampleSize);
		log(" numberOfClusters",numberOfClusters);
		log(" maxIter",maxIter);
		log(" number-of-functions",L);
		long start = System.currentTimeMillis();
		ArrayList<LSHFunction<T>> lshfuns=new ArrayList<LSHFunction<T>>();
		for(int i=0;i<L;i++){
			start = System.currentTimeMillis();
			log(" Started sampling points");
			log("L-run",i);
			ArrayList<T> sample= Sampling.randomSample(data, sampleSize);
			logTime("  sampling "+sample.size()+" data poins ",start);
			
			log(" started k-medoid");
			start = System.currentTimeMillis();
			ArrayList<Cluster<T>> initClusters = ClusteringAlgorithm.fastKMedoidInit(sample, numberOfClusters);
			
			FastKMedoid<T> kmedoid = new FastKMedoid<T>();
			kmedoid.cluster(
					sample
					,numberOfClusters, maxIter,initClusters);
			
	        logTime(" k-medoid ",start);
	        KLSHFunction<T> fun = new KLSHFunction<T>(kmedoid.getClusters());
	        fun.saveClustersFile("centroid-kmedoidKmInitK"+numberOfClusters+"sample"+sampleSize+"L"+i);
	        lshfuns.add(fun);
		}

		// 2) Use cluster from previous step to create a KLSH function
        log(" created LSHMedoidFunction");
        start = System.currentTimeMillis();
		LSH<T> lsh=new LSH<T>(lshfuns);
		logTime(" creating LSHMedoidFunction",start);

		processAndQuery(lsh);

    }
    
    public void runExperimentKMedoidInitKpp(
    		int sampleSize,
    		int numberOfClusters,
    		int maxIter,
    		int L) 
    				throws IOException{
    	
    	// random init
    	// 0) Pre-process files in disk and load dataset, queries and ground truth in primary memory
    	loadFiles();
				
		
		// 1) Sample part of the data and running K-Medoid
				
		log(" Experiment setup");
		log(" DatasetSize",data.size());
		log(" sampleSize",sampleSize);
		log(" numberOfClusters",numberOfClusters);
		log(" maxIter",maxIter);
		log(" number-of-functions",L);
		long start = System.currentTimeMillis();
		ArrayList<LSHFunction<T>> lshfuns=new ArrayList<LSHFunction<T>>();
		for(int i=0;i<L;i++){
			start = System.currentTimeMillis();
			log(" Started sampling points");
			log("L-run",i);
			ArrayList<T> sample= Sampling.randomSample(data, sampleSize);
			logTime("  sampling"+sample.size()+" data poins ",start);
			
			log(" started k-medoid");
			start = System.currentTimeMillis();

			ArrayList<Cluster<T>> initClusters = ClusteringAlgorithm.chooseInitialCentersKMeansPP(sample, numberOfClusters);

			FastKMedoid<T> kmedoid = new FastKMedoid<T>();
			kmedoid.cluster(
					sample
					,numberOfClusters, maxIter,initClusters);
			
	        logTime(" k-medoid ",start);
	        KLSHFunction<T> fun = new KLSHFunction<T>(kmedoid.getClusters());
	        fun.saveClustersFile("centroid-kmedoidKppInitK"+numberOfClusters+"sample"+sampleSize+"L"+i);
	        lshfuns.add(fun);
		}

		// 2) Use cluster from previous step to create a KLSH function
        log(" created LSHMedoidFunction");
        start = System.currentTimeMillis();
		LSH<T> lsh=new LSH<T>(lshfuns);
		logTime(" creating LSHMedoidFunction",start);

		processAndQuery(lsh);

    }
    /**
     * runs the fast kmedoid version and uses thoses centers to make a LSH function
     * @param sampleSize
     * @param numberOfClusters
     * @param maxIter
     * @param L
     * @param W
     * @param init, 0 is for random initialization, 1 is for the proposed initialization in Park and Jun, 2 is for Kmeans++ initialization
     * @throws IOException
     */
    
    /*@SuppressWarnings("static-access")
	public void runExperimentKMedoid(
    		int sampleSize,
    		int numberOfClusters,int maxIter,
    		int L,int W,int resamplesize, int init) 
    				throws IOException{
    	
    	// 0) Pre-process files in disk and load dataset, queries and ground truth in primary memory
    	loadFiles();
				
		
		// 1) Sample part of the data and running K-Medoid
				
		log(" Experiment setup");
		log(" DatasetSize",data.size());
		log(" sampleSize",sampleSize);
		log(" numberOfClusters",numberOfClusters);
		log(" maxIter",maxIter);
		log(" numberofclusters-per-function ",W);
		log(" number-of-functions ",L);
		
		long start = System.currentTimeMillis();
		log(" Started sampling points");
		ArrayList<EuclideanIntegerPoint> sample= Sampling.randomSample(data, sampleSize);
		logTime("  sampling"+sample.size()+" data poins ",start);
		
		
		ArrayList<LSHFunction<EuclideanIntegerPoint>> lshfuns=new ArrayList<LSHFunction<EuclideanIntegerPoint>>();
		for(int i=0;i<L;i++){
			log(" started k-medoid");
			start = System.currentTimeMillis();
			ClusteringAlgorithm<EuclideanIntegerPoint> kmedoid=new FastKMedoid<EuclideanIntegerPoint>();
			ArrayList<Cluster<EuclideanIntegerPoint>> initClusters;
			switch(init){
				case 0:
					kmedoid.cluster(sample,resamplesize, maxIter); //classic random initialization
					break;
				case 1:
					initClusters = ClusteringAlgorithm.fastKMedoidInit(sample, resamplesize); // initialization method of clustering algorithm taken from "A Simple and fast algorithm for K-medoids clustering" by Park and Jun [Expert Systems with Applications] 
					kmedoid.cluster(sample,resamplesize, maxIter,initClusters);
					break;
				case 2:
					initClusters = ClusteringAlgorithm.chooseInitialCentersKMeansPP(sample, resamplesize); // kmeans++ initialization (taken from apache.commons.math)
					kmedoid.cluster(sample,resamplesize, maxIter,initClusters);
					
					break;
				case 3://just random initiation
					kmedoid=new RandomMedoid<EuclideanIntegerPoint>();
					kmedoid.cluster(
							sample
							,resamplesize);
					break;
			}
				
			
	        logTime(" k-medoid ",start);

			// 2) Use cluster from previous step to create a KLSH function
	        log(" created LSHMedoidFunction");
	        start = System.currentTimeMillis();
	        
	        
	        KLSHResampleFunction<EuclideanIntegerPoint> functions= new KLSHResampleFunction<EuclideanIntegerPoint>(kmedoid.clusters,W,numberOfClusters,3000);
	       
			
	        lshfuns.add(functions);
	        
	        logTime(" creating LSHMedoidFunction",start);
		}
		
		LSH<EuclideanIntegerPoint> lsh=new LSH<EuclideanIntegerPoint>(lshfuns,L);
		processAndQuery(lsh);// the mode=1 was introduced exclusively due to this new mechanism of resampling introduced now
		
    }*/
    /**
     * run experiment using Metric LSH (our second proposed method)
     * @param sampleSize
     * @param numberOfClusters
     * @param maxIter
     * @param L
     * @param W
     * @param resamplesize
     * @throws IOException
     */
    public void runExperimentLSHMetric2(
    		int sampleSize,
    		int numberOfClusters,int maxIter,
    		int L,int W,int resamplesize) 
    				throws IOException{
    	
    	// 0) Pre-process files in disk and load dataset, queries and ground truth in primary memory
    	loadFiles();
				
		
		// 1) Sample part of the data and running K-Medoid
				
		log(" Experiment setup");
		log(" DatasetSize",data.size());
		log(" sampleSize",sampleSize);
		log(" numberOfClusters",numberOfClusters);
		log(" reSampleSize",resamplesize);
		log(" maxIter",maxIter);
		log(" numberofclusters-per-function ",W);
		log(" number-of-functions ",L);
		
		long start = System.currentTimeMillis();
		log(" Started sampling points");
		ArrayList<T> sample= Sampling.randomSample(data, sampleSize);
		logTime("  sampling"+sample.size()+" data poins ",start);
		
		
		ArrayList<LSHFunction<T>> lshfuns=new ArrayList<LSHFunction<T>>();
		for(int i=0;i<L;i++){
			log(" started lshmetric");
			start = System.currentTimeMillis();
			ArrayList<Integer> clusteridx=Sampling.randomIndex(sample.size(), resamplesize);
			
			
	        logTime(" lshmetric",start);

			// 2) Use cluster from previous step to create a KLSH function
	        log(" created LSHMedoidFunction");
	        start = System.currentTimeMillis();
	        
	        
	        KLSHResampleFunction<T> function= new KLSHResampleFunction<T>(clusteridx,sample,W,numberOfClusters,30000);
	       
			
	        lshfuns.add(function);
	        
	        logTime(" creating LSHMedoidFunction",start);
		}
		
		LSH<T> lsh=new LSH<T>(lshfuns,L);
		processAndQuery(lsh);// the mode=1 was introduced exclusively due to this new mechanism of resampling introduced now
		
    }
    
   
    public void runExperimentKMeans(
    		int sampleSize,
    		int numberOfClusters,
    		int maxIter,int L) 
    				throws IOException{
    	// 0) Pre-process files in disk and load dataset, queries and ground truth in primary memory
    	loadFiles();
		
		// 1) Sample part of the data and running K-Means
		
		
		log(" Experiment setup");
		
		log(" DatasetSize",data.size());
		log(" sampleSize",sampleSize);
		log(" numberOfClusters",numberOfClusters);
		log(" maxIter",maxIter);
		log(" number-of-functions",L);
		long start = System.currentTimeMillis();
		ArrayList<LSHFunction<T>> lshfuns=new ArrayList<LSHFunction<T>>();

		for(int i=0;i<L;i++){
		
			log(" Started sampling points");
			ArrayList<T> sample= Sampling.randomSample(data, sampleSize);
			logTime("  sampling"+sample.size()+" data poins ",start);
			
			log(" started k-means");
			start = System.currentTimeMillis();
			KMeansPlusPlusClusterer<T> kmeans=new KMeansPlusPlusClusterer<T>(new Random());
			
			List<org.apache.commons.math3.stat.clustering.Cluster<T>> tempclusters = kmeans.cluster(
					sample
					,numberOfClusters, maxIter);
			ArrayList<Cluster<T>> kclusters = new ArrayList<Cluster<T>>(tempclusters.size());
			for(org.apache.commons.math3.stat.clustering.Cluster<T> ccluster:tempclusters){
				kclusters.add(new Cluster<T>(ccluster.getCenter()));
			}
			
			logTime(" k-means ",start);
			KLSHFunction<T> fun = new KLSHFunction<T>(kclusters);
	        fun.saveClustersFile("centroid-kmeansKppK"+numberOfClusters+"sample"+sampleSize+"L"+i);
	        lshfuns.add(fun);
	        
		}
		// 2) Use cluster from previous step to create a KLSH function
        log(" created LSHMedoidFunctions");
        start = System.currentTimeMillis();
		LSH<T> lsh=new LSH<T>(lshfuns,L);
		logTime(" creating LSHKMeansFunction",start);

		processAndQuery(lsh);

    }
/*
    public void runExperimentE2(int l,int k,int w) 
    				throws IOException{
    	// 0) Pre-process files in disk and load dataset, queries and ground truth in primary memory
    	loadFiles();
				
		// 1) Sample part of the data and running K-Medoid
				
		log(" Experiment setup");
		log(" DatasetSize",data.size());
		log(" started E2LSH");
		long start = System.currentTimeMillis();
		
        log(" creating LSHFunction");
        E2LSHFunction e2lsh;//=new E2LSHFunction();
        //E2LSHFunction.E2LSHTuning.tune(100,1.3,data.size());
        ArrayList<LSHFunction<EuclideanIntegerPoint>> lshfunlist
    	=new ArrayList<LSHFunction<EuclideanIntegerPoint>>(l);
        for(int i=0;i<l;i++){
	        e2lsh = new E2LSHFunction(
	        		w,
	        		k,
	        		128,
	        		new NormalDistribution(),
	        		new UniformRealDistribution(0,w));
	        lshfunlist.add(e2lsh);
        }
        
		LSH<EuclideanIntegerPoint> lsh=new LSH<EuclideanIntegerPoint>(lshfunlist,k);
		
		logTime(" created LSHFunction",start);

		processAndQuery(lsh);

    }
    */
    
    /**
     * Based on "Robust and Efﬁcient Locality Sensitive Hashing for Nearest Neighbor Search in Large Data Sets" BigLearn2012 Workshop (NIPS), [Kang,Jung]
     * @param numberOfClusters
     * @throws IOException
     */
    public void runExperimentDFLSH(
    		int numberOfClusters) 
    				throws IOException{
    	
    	// 0) Pre-process files in disk and load dataset, queries and ground truth in primary memory
    	loadFiles();
				
		
		// 1) Sample part of the data and running K-Medoid
				
		log(" Experiment setup");
		log(" DatasetSize",data.size());
		log(" numberOfClusters",numberOfClusters);
		
		long start = System.currentTimeMillis();

		log(" started random-medoid");
		start = System.currentTimeMillis();
		RandomMedoid<T> rmedoid = new RandomMedoid<T>();
		rmedoid.cluster(
				data
				,numberOfClusters);
		
        logTime(" random-medoid ",start);

		// 2) Use cluster from previous step to create a KLSH function
        log(" created LSHMedoidFunction");
        start = System.currentTimeMillis();
		LSH<T> lsh=new LSH<T>(
				new KLSHFunction<T>(rmedoid.getClusters()),1);
		logTime(" creating LSHMedoidFunction",start);

		processAndQuery(lsh);

    }
    
    public void runExperimentDFLSH(
    		int sampleSize,
    		int L,int numberOfClusters) 
    				throws IOException{
    	
    	// 0) Pre-process files in disk and load dataset, queries and ground truth in primary memory
    	loadFiles();
				
		
		// 1) Sample part of the data and running K-Medoid
				
		log(" Experiment setup");
		log(" DatasetSize",data.size());
		log(" sampleSize", sampleSize);
		log(" numberOfClusters",numberOfClusters);
		log(" number-of-functions",L);
		
		long start = System.currentTimeMillis();

		log(" started random-medoid");
		start = System.currentTimeMillis();
		RandomMedoid<T> rmedoid = new RandomMedoid<T>();
		rmedoid.cluster(
				data
				,sampleSize);
		
        logTime(" random-medoid ",start);

		// 2) Use cluster from previous step to create a KLSH function
        log(" created LSHMedoidFunction");
        start = System.currentTimeMillis();
        log(" numberofclusters-per-function ",numberOfClusters);
				
		ArrayList<LSHFunction<T>> functions= new ArrayList<LSHFunction<T>>(L);
        for(int i=0;i<L;i++){
        	ArrayList<Cluster<T>> cc=Sampling.randomSample(rmedoid.getClusters(),numberOfClusters);
        	
        	KLSHFunction<T> fun = new KLSHFunction<T>(cc);
	        fun.saveClustersFile("centroid-DFLSH"+numberOfClusters+"sample"+sampleSize+"L"+i);
        	functions.add(fun);
        }
        
		LSH<T> lsh=new LSH<T>(functions);
		logTime(" creating LSHMedoidFunction",start);

		processAndQuery(lsh);

    }
 
    /**
     * Runs a experiment taking arguments from args (command-line) and choosing from a pre-set possible experiment (enum Experiment) and 
     * using a instance of LSHTest with the appropriate setting for the experiment (data/query type, dataset source, queries set source) 
     * @param args
     * @param experiment
     * @param lshtest
     * @throws IOException
     */
    public static <T extends Clusterable<T>> void runExperiment(String[] args, Experiment experiment, LSHTest<T> lshtest) throws IOException {
    	int sampleSize = 2000;
		int numberOfClusters = 1000;
		int maxIter = 100;
		int L = 1;
		int W = 30;
		int resample=10000;
		if(args.length > 2){
			sampleSize = (int)Double.parseDouble(args[2]);
			if(args.length > 3){
				numberOfClusters = (int)Double.parseDouble(args[3]);
				if(args.length > 4){
					maxIter = (int)Double.parseDouble(args[4]);
					if(args.length > 5){
						L = (int)Double.parseDouble(args[5]);
						if(args.length > 6){
							resample = (int)Double.parseDouble(args[6]);
							if(args.length > 7){
								W = (int)Double.parseDouble(args[7]);
							}
						}
					}
				}
			}
		}
		ClusteringAlgorithm.resetCounter();
		LSHTest.kNN=maxIter;
		
		switch(experiment){
			case KMeans:
				lshtest.runExperimentKMeans(sampleSize, numberOfClusters, maxIter,  L);
				break;
			case LSHMetric:
				lshtest.runExperimentLSHMetric2(sampleSize,numberOfClusters,maxIter,L,W,resample);
				break;
			case KMedoidInitParkJun:
				lshtest.runExperimentKMedoidInit(sampleSize,numberOfClusters,maxIter,L);
				break;
			case KMedoidInitKpp:
				lshtest.runExperimentKMedoidInitKpp(sampleSize,numberOfClusters,maxIter,L);
				break;
			case KMedoidInitClassic:
				lshtest.runExperimentKMedoid(sampleSize,numberOfClusters,maxIter,L); // classic lshmedoid 
				break;
			case DFLSH:
				// samplesSize is the number of cluster for DFLSH
				lshtest.runExperimentDFLSH(sampleSize,L,numberOfClusters);
				break;
			default:
				LSHTest.log("unknown experiment");
				break;
		}
		
		
		//log(" numberOfDistanceCalculations=",ClusteringAlgorithm.getCountdist());
    }
	/**
	 * @param args
	 */

	public static void main(String[] args) {
		try{
			LSHTest<EuclideanIntegerPoint> lshtest=new LSHTest<EuclideanIntegerPoint>(args[0],args[1],new APMIntegerPointsReader());
			
			runExperiment(args,Experiment.DFLSH,lshtest);
			
		}catch(IndexOutOfBoundsException e)
		{
			e.printStackTrace();
			System.err.println("You must provide dataset file path and query file path");
		}catch(Exception e1)
		{
			e1.printStackTrace();
		}
		
	}
	public static enum Experiment { KMeans, LSHMetric, KMedoidInitParkJun, KMedoidInitKpp, KMedoidInitClassic, DFLSH};


    public static void log(String str){
    	if(logStatistics)
    		System.err.println("## "+str);
    }
    public static void log(String varName, Object num){
    	if(logStatistics)
    		System.err.println("#v# "+varName+"="+num);
    }
    public static void log(String varName, double num){
    	if(logStatistics)
    		System.err.println("#v# "+varName+"="+num);
    }
    public static void log(String varName, int num){
    	if(logStatistics)
    		System.err.println("#v# "+varName+"="+num);
    }
    public static void logTime(String str, long start){
    	if(logStatistics)
            System.err.println("#time# ended "+str+" after:"+(System.currentTimeMillis()-start)+"ms");
    }
    public static void log(String[] varnames, int[] nums){
    	if(logStatistics){
    		if(varnames!=null && nums!=null && varnames.length == nums.length){
    			System.err.print("#v#");
    			for(int i=0;i<nums.length;i++){
    				System.err.print(" "+varnames[i]+"="+nums[i]);
    			}
    			System.err.println(" ");
    		}
    	}
    }
    public static void log(String[] varnames, Object[] nums){
    	if(logStatistics){
    		if(varnames!=null && nums!=null && varnames.length == nums.length){
    			System.err.print("#v# ");
    			for(int i=0;i<nums.length;i++){
    				System.err.print(" "+varnames[i]+"="+nums[i]);
    			}
    			System.err.println(" ");
    		}
    	}
    }
    public static void log(String varnames, Object[] nums){
    	if(logStatistics){
    		if(varnames!=null && nums!=null){
    			System.err.print("#v# ");
    			System.err.print(varnames+"=[");
    			for(int i=0;i<nums.length;i++){
    				System.err.print(" "+nums[i]);
    			}
    			System.err.println("] ");
    		}
    	}
    }
    public static <T> void log(String varnames, List<T> nums){
    	if(logStatistics){
    		if(varnames!=null && nums!=null){
    			System.err.print("#v# ");
    			System.err.print(varnames+"=[");
    			for(T num:nums){
    				System.err.print(" "+num);
    			}
    			System.err.println("] ");
    		}
    	}
    }

}
