package lshmedoid.test;

import java.util.Comparator;

import org.apache.commons.math3.stat.clustering.Clusterable;

import lshmedoid.cluster.Cluster;

public class NumberOfPointsClusterComparator<T extends Clusterable<T>>  implements Comparator<Cluster<T>> {

	@Override
	public int compare(Cluster<T> o1, Cluster<T> o2) {
		// compare o1 to o2 according to their number of points
		if(o1.getPoints().size()==o2.getPoints().size())
			return 0;
		
		return o1.getPoints().size()<o2.getPoints().size()?-1:1;
	}

}
