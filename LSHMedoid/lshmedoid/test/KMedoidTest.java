/**
 * 
 */
package lshmedoid.test;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import lshmedoid.cluster.Cluster;
import lshmedoid.cluster.ClusteringAlgorithm;
import lshmedoid.cluster.FastKMedoid;
import lshmedoid.cluster.PAMKMedoid;
import lshmedoid.lsh.QueryOrderingComparator;

import org.apache.commons.math3.stat.clustering.Clusterable;
import org.apache.commons.math3.stat.clustering.EuclideanDoublePoint;
/**
 * @author eliezer
 *
 */
public class KMedoidTest {

	/**
	 * 
	 */
	public KMedoidTest() {
		// TODO Auto-generated constructor stub
	}
	
	public static ArrayList<EuclideanDoublePoint> loadDataset(String filename){
		ArrayList<EuclideanDoublePoint> points = new ArrayList<EuclideanDoublePoint>();
		BufferedReader in = null;

	    try {
	        in = new BufferedReader(new FileReader(filename));
	        String line = null;
	        while ((line = in.readLine()) != null) {
	        	ArrayList<Double> point=new ArrayList<Double>();
				Scanner linescan = new Scanner(line);
				
				while (linescan.hasNext()){
					double num = Double.parseDouble(linescan.next());
					//System.out.println(num);
					point.add(new Double(num));
				}
				
	        	
	        	linescan.close();
	        	double[] p = new double[point.size()];
	        	
	        	for(int i=0;i<p.length;i++){
	        		p[i]=point.get(i).doubleValue();
	        	}
	        	points.add(new EuclideanDoublePoint(p));
	        }
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (in != null) {
	            try {
	                in.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
		
		
		return points;
		
	}
	public static <T extends Clusterable<T>>  void testClusteringAlgorithm(ClusteringAlgorithm<T> clus,ArrayList<T> points, int k, int iter){
		long start = System.currentTimeMillis();
		int[] assignments=clus.cluster(points, k, iter);
		LSHTest.logTime("FastKMedoid ",start);
		//assignments=kmedoid.distmatrixCluster(points, 20, 100);
		ArrayList<Cluster<T>>  cclusters1 = clus.getClusters();
		try {
			ClusteringAlgorithm.addPointsToClusters(points,cclusters1 , assignments);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Collections.sort(cclusters1, new NumberOfPointsClusterComparator<T>());
		/*for(int a : assignments){
			System.out.println(a);
		}*/
		clus.setClusters(cclusters1);
		System.out.println(clus.toString());
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClusteringAlgorithm<EuclideanDoublePoint> kmedoid = new PAMKMedoid<EuclideanDoublePoint>();
		ClusteringAlgorithm<EuclideanDoublePoint> fastmedoid = new FastKMedoid<EuclideanDoublePoint>();
		ArrayList<EuclideanDoublePoint> points = new ArrayList<EuclideanDoublePoint>(10);
		int[] assignments;
		if(args.length >= 1){
			String fileName; // "/home/eliezers/datasets/clustering/yeast-8d-k10-n1484.txt"; "http://cs.joensuu.fi/sipu/datasets/KDDCUP04Bio.txt";
			// /net/nfs2.fee.unicamp.br/export/homes/vol1/DCA/home/msc/eliezers/datasets/clustering/a1.txt
			fileName = args[0];
			points = loadDataset(fileName);
			LSHTest.log(" dataset size",points.size());
			
			/*********PAMKMedoid*******/
			//testClusteringAlgorithm(kmedoid,points,20,100);
			
			/*********FastKMedoid******/
			testClusteringAlgorithm(fastmedoid ,points,20,100);
			
		}else{
			System.err.println(" missing dataset file");
		}
		
		
	}

}
