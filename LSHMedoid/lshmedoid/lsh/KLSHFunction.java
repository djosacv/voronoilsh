package lshmedoid.lsh;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import lshmedoid.cluster.Cluster;
import lshmedoid.cluster.ClusteringAlgorithm;
import lshmedoid.test.LSHTest;

import org.apache.commons.math3.stat.clustering.Clusterable;
import org.apache.commons.math3.util.Pair;

public class KLSHFunction <T extends Clusterable<T>> implements LSHFunction<T>{
	private ArrayList<Cluster<T>> clusters;
	public KLSHFunction(ArrayList<Cluster<T>> arrayList) {
		this.clusters=arrayList;
		//for(int i=0;i<this.clusters.size();i++){
		//	LSHTest.log(" cluster_"+i,this.clusters.get(i));
		//}
	}
	
	public void loadClustersFile(String filename){
		//TODO
	}
	public void saveClustersFile(String filename){
		try {
			BufferedWriter fo = new BufferedWriter(new FileWriter(filename));

			for(Cluster<T> cc:clusters){
				fo.write(cc.toString()+"\n");
			}
			fo.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public int hash(T x) {
		Pair<Integer,Double> p= ClusteringAlgorithm.getNearestClusterDistance(clusters,x);
		ClusteringAlgorithm.error+=p.getSecond();
		return p.getFirst();
	}

	@Override
	public int[] hashg(T x) {
		// TODO Auto-generated method stub
		int[]  res = new int[1];
		res[0] = hash(x);
		return res;
	}

	@Override
	public int[] hashsequence(T x, int numprobes) {
		// TODO Auto-generated method stub
		ArrayList<T> clusterlist=new ArrayList<T>();
		ArrayList<Integer> idxclusterlist=new ArrayList<Integer>();
		
		clusterlist.ensureCapacity(this.clusters.size());
		idxclusterlist.ensureCapacity(this.clusters.size());
		
		int i=0;
		for(Cluster<T> clust:this.clusters){
			clusterlist.add(clust.getCenter());
			idxclusterlist.add(new Integer(i));
			i++;
		}
		Collections.sort(idxclusterlist, new QueryOrderingComparator<T>(x,clusterlist));
		
		int[] result=new int[Math.min(numprobes, idxclusterlist.size())];
		for(int j=0;j<idxclusterlist.size() && j<numprobes;j++){
			result[j]=idxclusterlist.get(j).intValue();
		}
		return result;
	}
}
