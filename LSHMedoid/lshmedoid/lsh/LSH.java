package lshmedoid.lsh;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lshmedoid.cluster.ClusteringAlgorithm;
import lshmedoid.test.LSHTest;

import org.apache.commons.math3.stat.clustering.*;

public class LSH <T extends Clusterable<T>>{
	private int l;
	private int k;
	ArrayList<LSHFunction<T>> lshfun;
	LSHFunction<T> lshfun1;
	/** a tabela hash tem dois valores o primeiro é hash gerado pela função LSH usando o dado
	 do tipo T. Cada bucket deste tem uma lista ordenada de inteiros, correspondentes ao indice 
	 no arraylist que foi passado para ser processado do dado. assim evitamos duplicação e montamos
	 uma estrutura de dados que ocupado menos espaço na memória.
	 
	 **/
	HashMap<Integer,LinkedList<Integer>> table;
	ArrayList<HashMap<Integer,LinkedList<Integer>>> table2;
	
	public LSH(ArrayList<LSHFunction<T>> lshfun,int k) {
		this.lshfun=lshfun;
		this.l=lshfun.size();
		this.k=k;
		this.table2=new ArrayList<HashMap<Integer,LinkedList<Integer>>>(this.l);
		for(int i=0;i<this.l;i++){
			this.table2.add(new HashMap<Integer,LinkedList<Integer>>());
		}
	}
	
	public LSH(ArrayList<LSHFunction<T>> lshfun) {
		this.lshfun=lshfun;
		this.l=lshfun.size();
		this.k=1;
		this.table2=new ArrayList<HashMap<Integer,LinkedList<Integer>>>(this.l);
		for(int i=0;i<this.l;i++){
			this.table2.add(new HashMap<Integer,LinkedList<Integer>>());
		}
	}
	public LSH(LSHFunction<T> lshfun,int L) {
		this.lshfun1=lshfun;
		ArrayList<LSHFunction<T>> lst=new ArrayList<LSHFunction<T>>();
		lst.add(lshfun);
		
		this.lshfun=lst;
		this.l=L;
		this.k=1;
		this.table=new HashMap<Integer,LinkedList<Integer>>();
		this.table2=new ArrayList<HashMap<Integer,LinkedList<Integer>>>(this.l);
		for(int i=0;i<this.l;i++){
			this.table2.add(new HashMap<Integer,LinkedList<Integer>>());
		}
		
	}



	/**
	 * implementação mais simples que usa somente um função LSH
	 * @param points
	 */
	public void processData1(List<T> points){
		int i=0;
		long start=System.currentTimeMillis();
		for(Iterator<T> iter = points.iterator();iter.hasNext(); i++){
			T point = iter.next();		
			Integer idx=new Integer(lshfun1.hash(point));
			if(!table.containsKey(idx)){
				LinkedList<Integer> lst= new LinkedList<Integer>();
				lst.add(new Integer(i));
				table.put(new Integer(idx),lst);
			}else{
				LinkedList<Integer> lst=table.get(idx);
				lst.add(new Integer(i));
			}
			if(i==0)
				LSHTest.logTime(" process a single point ", start);
		}
	}
	/**
	 * implementacao mais simples que busca usando somente uma função LSH
	 * @param query
	 * @return
	 */
	public LinkedList<Integer> shortIndexList1(T query){
		Integer idx=new Integer(lshfun1.hash(query));
		return table.get(idx);
	}
	
	
	
	/**
	 * implementação que usa uma lista de L funções LSH e dois modos de fazer o hash para estas L funções
	 *  (um hash que retorna L valores ou L chamadas de funções hash diferentes)
	 * @param points
	 * @parm mode
	 */
	public void processData(List<T> points,int mode){
		int i=0;
		///if(mode == 0){
			
			for(int j=0;j<table2.size();j++){
				HashMap<Integer,LinkedList<Integer>> table=table2.get(j);
				LSHFunction<T> lshfunj=lshfun.get(j);
				ClusteringAlgorithm.error=0.0;
				i=0;
				long start=System.currentTimeMillis();
				for(Iterator<T> iter = points.iterator();iter.hasNext(); i++){
					T point = iter.next();
					
					if(point!=null){
						
						
						Integer idx=new Integer(lshfunj.hash(point));
						//LSHTest.log(" hash idx",idx.intValue());
						if(!table.containsKey(idx)){
							LinkedList<Integer> lst= new LinkedList<Integer>();
							lst.add(new Integer(i));
							table.put(new Integer(idx),lst);
						}else{
							LinkedList<Integer> lst=table.get(idx);
							lst.add(new Integer(i));
						}
						if(i==0)
							LSHTest.logTime(" process a single point ", start);
					}
				}
				LSHTest.log("ClusteringAlgorithm.error",ClusteringAlgorithm.error);
			}
		/*}
		if(mode == 1){
			long start=System.currentTimeMillis();

			for(Iterator<T> iter = points.iterator();iter.hasNext(); i++){
				T point = iter.next();
				if(point!=null){
					int[] idxg=lshfun1.hashg(point);
					if(idxg!=null){
						
					
						for(int j=0;j<idxg.length;j++){
							HashMap<Integer,LinkedList<Integer>> table=table2.get(j);
							Integer idx = new Integer(idxg[j]);
							
							//LSHTest.log(" hash idx",idx.intValue());
							if(!table.containsKey(idx)){
								LinkedList<Integer> lst= new LinkedList<Integer>();
								lst.add(new Integer(i));
								table.put(new Integer(idx),lst);
							}else{
								LinkedList<Integer> lst=table.get(idx);
								lst.add(new Integer(i));
							}
							if(i==0)
								LSHTest.logTime(" process a single point ", start);
						}
					}
				}

			}
			
		}*/
	}
	/**
	 * implementacao que busca usando uma lista de funções lsh.
	 * @param query
	 * @param mode - if mode = 0 it use a list of L single valued hash functions, if mode = 1, it uses a unique multivalued hash function
	 * @return
	 */
	public LinkedList<Integer> shortIndexList(T query,int mode){
		LinkedList<Integer> result=new LinkedList<Integer>();
		//LSHTest.log(" query",query);
		/*if(mode==1){
			try{
				int[] idxg=lshfun1.hashg(query);
				this.l = idxg.length;
				for(int i=0;i<idxg.length;i++){
					Integer idx=new Integer(idxg[i]);
					try{
						LSHTest.log(new String[]{" bin"," size"},new Object[]{idx,table2.get(i).get(idx).size()});
						//result.addAll(table2.get(i).get(idx));
						for(Integer ipoint:table2.get(i).get(idx)){
							result.add(ipoint);
							if(this.l>=10)
							{
								if(result.size()>=3*this.l)
									return result;
							}
						}
					}catch(Exception e2){
						LSHTest.log(" empty bin idx"+i,idx.intValue());
						return result;
					}
					
				}
			}catch(Exception e){
				LSHTest.log(" empty bin");
				return result;
			}
		}
		if(mode==0){*/
			try{
				for(int i=0;i<lshfun.size();i++){
					Integer idx=new Integer(lshfun.get(i).hash(query));
					
					HashMap<Integer, LinkedList<Integer>> ltable = table2.get(i);
					try{
						LSHTest.log(new String[]{" bin"," size"},new Object[]{idx,ltable.get(idx).size()});
						
						//result.addAll(table2.get(i).get(idx));
						//LSHTest.log("shorlist L"+i,table2.get(i).get(idx));
						for(Integer ipoint:ltable.get(idx)){
							result.add(ipoint);
						}
						/*if(this.l>3)
						{
							if(result.size()>=20*this.l)
								return result;
						}*/
					}catch(Exception e2){
						LSHTest.log(" empty bin idx"+i,idx.intValue());
					}
				}
			}catch(Exception e){
				LSHTest.log(" empty bin");
				return result;
			}
			
		//}
		
		return result;
	}
	
	public LinkedList<Integer> multiprobeShortIndexList(T query, int probes){
		LinkedList<Integer> result=new LinkedList<Integer>();
		LSHTest.log(" numprobes",probes);
		
			try{
				for(int i=0;i<lshfun.size();i++){
					HashMap<Integer, LinkedList<Integer>> ltable = table2.get(i);
					int[] probesequence=lshfun.get(i).hashsequence(query,probes);
					

					for (int iidx:probesequence){
						Integer idx=new Integer(iidx);
						
						try{
							LSHTest.log(new String[]{" bin"," size"},new Object[]{idx,ltable.get(idx).size()});
							for(Integer ipoint:ltable.get(idx)){
								result.add(ipoint);
							}
						}catch(Exception e2){
							LSHTest.log(" empty bin idx"+i,idx.intValue());
						}
					}
				}
			}catch(Exception e){
				LSHTest.log(" empty bin");
				return result;
			}
			
		//}
		
		return result;
	}
	/**
	 * this query function is suitable when we have only one function. It's a special case
	 * and should be used carefully
	 * @param query
	 * @param dataset
	 * @param k
	 * @return
	 */
	public LinkedList<Integer> query(T query,ArrayList<T> dataset, int k){
		HashSet<Integer> tempset = new HashSet<Integer>(this.shortIndexList1(query));
		return queryFinalScan(query,dataset,tempset,k);
	}
	/**
	 * this query function is the more general query strategy. It generates a short list using shortIndexList
	 *  
	 * @param query
	 * @param dataset
	 * @param k
	 * @return
	 */
	public LinkedList<Integer> query2(T query,ArrayList<T> dataset, int k,int mode){
		 
		
		HashSet<Integer> tempset = new HashSet<Integer>(shortIndexList(query, mode));
		return queryFinalScan(query,dataset,tempset,k);
	}
	/**
	 * 
	 * @param query
	 * @param dataset
	 * @param k
	 * @param numprobes
	 * @return
	 */
	public LinkedList<Integer> queryMultiProbe(T query,ArrayList<T> dataset, int k,int numprobes){
		HashSet<Integer> tempset = new HashSet<Integer>(multiprobeShortIndexList(query,numprobes));
		return queryFinalScan(query,dataset,tempset,k);
	}
	
	public LinkedList<Integer> queryFinalScan(T query,ArrayList<T> dataset,HashSet<Integer> candidateSet, int k){
		LinkedList<Integer> idxshortlst = new LinkedList<Integer>(candidateSet);// take off all repeated elements
		LSHTest.log(" shortlistSize",idxshortlst.size());
		Collections.sort(idxshortlst, new QueryOrderingComparator<T>(query,dataset));
		if(k < idxshortlst.size())
			return new LinkedList<Integer>(idxshortlst.subList(0, k));
		return idxshortlst;
	}
	
	public void stats(){
		LSHTest.log("-lsh LSH Stats "+lshfun.get(0).getClass().getSimpleName());
		LSHTest.log("-lsh L",this.l);
		LSHTest.log("-lsh k",this.k);
		LSHTest.log("-lsh list-of-tables-size",this.table2.size());
		int i=0;
		for(HashMap<Integer,LinkedList<Integer>> htab:this.table2){
			LSHTest.log("-lsh Table"+i+" numberOfKeys"+htab.size());
			for(Integer key:htab.keySet()){
				LSHTest.log("-lsh key="+key+" bucketsize",htab.get(key).size());
			}
			i++;
		}
		
	}
}
