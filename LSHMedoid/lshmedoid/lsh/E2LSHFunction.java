/**
 * 
 */
package lshmedoid.lsh;

import java.util.LinkedList;


import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;
import org.apache.commons.math3.stat.clustering.EuclideanIntegerPoint;

/**
 * this implements the so called Euclidean LSH function (Datar,2004)
 * @author eliezer
 *
 */
public class E2LSHFunction implements LSHFunction<EuclideanIntegerPoint> {
	public int w;// bin width
	public int k;// key length
	public int d;// data is embedded in R^d
	public double[][] A;
	public double[] b;
	/**
	 * 
	 */
	static public class E2LSHTuning{
		static public double p1;
		static public double p2;
		static public int L;
		static public double rho;
		static public int K;
		static public int n;
		
		public static void tune(double R,double c,int n1){
			n=n1;
			
			p1=p(R,1);
			p2=p(R,c);
			rho=Math.log(1/p1)/Math.log(1/p2);
			K=(int)Math.ceil(Math.log(n)/Math.log(1/p1));
			L=(int)Math.ceil(Math.pow(n, rho));
		}
		static double p(double r, double c){
			NormalDistribution norm=new NormalDistribution();
			double rc=r/c;
			double res=1-norm.cumulativeProbability(-rc)-(2.0/(Math.sqrt(2*Math.PI)*rc))*(1-Math.exp(-0.5*rc*rc));
			return res;
		}
		
		
	}
	public E2LSHFunction(int w,int k,int d,NormalDistribution Anumbers,UniformRealDistribution Bnumbers) {
		this.w=w;
		this.k=k;
		this.d=d;
		A=new double[k][d];
		b=new double[k];
		for(int i=0;i<k;i++){
			for(int j=0;j<d;j++){
				A[i][j]=Anumbers.sample();
			}
			b[i]=Bnumbers.sample();
		}
	}
	public E2LSHFunction(){
	}
	

	/**
	 * get the array representing the k-key bin address of the point. 
	 * this is usefull for latter implementation of MultiProbe.
	 * @param point
	 * @return
	 */
	public int[] getBin(int[] point){
		int[] res=null;
		if(point.length==this.d){
			res=new int[this.k];
			for(int i=0;i<this.k;i++){
				double Axi=0;
				double fxi=0;
				for(int j=0;j<this.d;j++){
					Axi+=A[i][j]*point[j];
				}
				fxi=(Axi+this.b[i])/this.w;
				//LSHTest.log(" fxi"+i,fxi);
				
				res[i]=(int) Math.floor(fxi);
				//LSHTest.log(" floor(fxi)"+i,res[i]);
			}
		}
		return res;
	}
	/**
	 Input: A Nx1 array (of integers)
	 Output: A 28 bit hash value.
	 From: http://stackoverflow.com/questions/2909106/
	 python-whats-a-correct-and-good-way-to-implement-hash/2909572#2909572
	 taken from https://github.com/zehsilva/Optimal-LSH/blob/master/lsh.py
	 * @return int hashvalue
	 */
	public int arrayHash(int[] arr){
		if(arr == null || arr.length==0)
			return 0;
		/*int value = arr[0] << 7;
		for(int i: arr)
			value = (101*value + i)& 0xfffffff;*/
		LinkedList<Integer> lst = new LinkedList<Integer>();
		for(int a:arr){
			lst.add(new Integer(a));
		}
		return lst.hashCode();
	}
	@Override
	public int hash(EuclideanIntegerPoint x) {
		if(x.getPoint().length==this.d){
			return arrayHash(getBin(x.getPoint()));
		}
		//LSHTest.log(" pointlength="+x.getPoint().length+" lsh.d",this.d);
		return 0;
	}
	public int[] hashg(EuclideanIntegerPoint x) {
		// TODO Auto-generated method stub
		int[]  res = new int[1];
		res[0] = hash(x);
		return res;
	}
	@Override
	public int[] hashsequence(EuclideanIntegerPoint x, int numprobes) {
		// TODO Auto-generated method stub
		return null;
	}

}
