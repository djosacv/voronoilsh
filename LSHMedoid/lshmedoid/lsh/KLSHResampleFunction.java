package lshmedoid.lsh;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

import lshmedoid.cluster.Cluster;
import lshmedoid.cluster.ClusteringAlgorithm;
import lshmedoid.test.LSHTest;
import lshmedoid.util.Sampling;

import org.apache.commons.math3.stat.clustering.Clusterable;
import org.apache.commons.math3.util.Pair;

public class KLSHResampleFunction <T extends Clusterable<T>> implements LSHFunction<T>{
	private ArrayList<Integer> clustersindex;
	private ArrayList<T> data;
	private int[][] subclusters;
	private int[] clusterSelected;
	static public double[] errorReSample;
	private int W,centerSetSize, Mtablesize;
	private int[] bhashkey;
	
	
	public KLSHResampleFunction(ArrayList<Integer> clusteridx,ArrayList<T> data, int W, int centerSetSize,int Mtablesize) {
		this.clustersindex=clusteridx;
		this.data=data;
		errorReSample = new double[W];
		this.bhashkey=new int[W];
		this.clusterSelected=new int[clustersindex.size()];
		
		this.centerSetSize=centerSetSize;
		this.W=W;
		
		this.Mtablesize=Mtablesize;
		
		subclusters = new int[W][this.centerSetSize];
		Random rnd = new Random();
		for(int i=0;i<W;i++){
			errorReSample[i]=0.0;
			this.bhashkey[i]=rnd.nextInt(this.Mtablesize);
			
			ArrayList<Integer> tmparr=Sampling.randomIndex(clustersindex.size(), this.centerSetSize);
			
			for(int j=0;j<this.centerSetSize;j++){
				subclusters[i][j]=tmparr.get(j).intValue();
				this.clusterSelected[subclusters[i][j]]=1;
			}
		}
		
	}
	
	public void loadClustersFile(String filename){
		//TODO
	}
	public void saveClustersFile(String filename){
		//TODO
	}



	@Override
	public int[] hashg(T x) {
		int[] res=new int[this.W];
		double[] dists=new double[clustersindex.size()];
		int i=0;
		for(int c:this.clusterSelected){
			if(c==1){
				dists[i]=x.distanceFrom(this.data.get(this.clustersindex.get(i)));
			}
			i++;
		}
		for(i=0;i<this.W;i++){
			double menor=Double.MAX_VALUE;
			int menorindex=0;
			int[] temparr = subclusters[i];
			for(int j=0;j<this.centerSetSize;j++){
				int idx = temparr[j];
				if(dists[idx]<=menor){
					menorindex=idx;
					menor=dists[idx];
				}
			}
			errorReSample[i]+=menor;
			res[i]=menorindex;
		}
		return res;
	}
	
	public int arrayHash(int[] arr){
		
		if(arr == null || arr.length==0)
			return 0;
		
		//int i=0,h=0;
		//if(arr.length == this.bhashkey.length)
		//for(int a:arr){

			//h += a * this.bhashkey[i];
			//i++;
		//}
		//h=arr.hashCode();
		
		//return h%this.Mtablesize;
		
		/*int value = arr[0] << 7;
		for(int i: arr)
			value = (101*value + i)& 0xfffffff;
		return value;*/
		
		LinkedList<Integer> lst = new LinkedList<Integer>();
		for(int a:arr){
			lst.add(new Integer(a));
		}
		return lst.hashCode();
	}
	
	public int hash(T x){
		return arrayHash(hashg(x));
	}

	@Override
	public int[] hashsequence(T x, int numprobes) {
		// TODO Auto-generated method stub
		return null;
	}
}
