package lshmedoid.lsh;

public interface  LSHFunction<T> {
	public int hash(T x);
	
	/**
	 * This method is intended to return a sequence of hash points nearby the central hashed point
	 * this sequence is to be utilized by multiprobe querying
	 * @param x
	 * @return
	 */
	public int[] hashsequence(T x,int numprobes);
	

	public int[] hashg(T x);
}
