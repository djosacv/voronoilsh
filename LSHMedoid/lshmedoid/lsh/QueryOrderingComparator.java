/**
 * 
 */
package lshmedoid.lsh;

import java.util.ArrayList;
import java.util.Comparator;

import org.apache.commons.math3.stat.clustering.Clusterable;

/**
 * @author eliezer
 *
 */

/**
 * This class implements a comparator interface. It implies in a ordering
 * that respect the distance of the points to the query, thus imposing a query-wise
 * sorting order.
 * @author eliezer
 *
 * @param <T>
 */
public class QueryOrderingComparator <T extends Clusterable<T>> 
	implements Comparator<Integer>{
	T query;
	ArrayList<T> dataset;
	/**
	 * 
	 */
	public QueryOrderingComparator(T q,ArrayList<T> dataset){
		this.query = q;
		this.dataset=dataset;
	}

	
	/**
	 * 
	 */
	public int compare(T o1, T o2) {
		// compare o1 to o2 according to their distance to the query
		if(o1.distanceFrom(this.query)==o2.distanceFrom(this.query))
			return 0;
		
		return o1.distanceFrom(this.query)<o2.distanceFrom(this.query)?-1:1;
	}

	@Override
	public int compare(Integer arg0, Integer arg1) {
		// TODO Auto-generated method stub
		return compare(this.dataset.get(arg0.intValue()),this.dataset.get(arg1.intValue()));
	}

}
