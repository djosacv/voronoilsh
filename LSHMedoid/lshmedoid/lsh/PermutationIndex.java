package lshmedoid.lsh;

public class PermutationIndex implements Comparable<PermutationIndex>{
	int ind;
	double dist;
	
	
	
	public PermutationIndex(int ind, double dist) {
		this.ind = ind;
		this.dist = dist;
	}
	/**
	 * @return the ind
	 */
	public int getInd() {
		return ind;
	}
	/**
	 * @param ind the ind to set
	 */
	public void setInd(int ind) {
		this.ind = ind;
	}
	/**
	 * @return the dist
	 */
	public double getDist() {
		return dist;
	}
	/**
	 * @param dist the dist to set
	 */
	public void setDist(double dist) {
		this.dist = dist;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(dist);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ind;
		return result;
	}

	public int compareTo(PermutationIndex o) {
		int cmp=Double.compare(this.dist, o.getDist());
		if(cmp==0){
			if (this.ind < o.getInd())
	            return -1;           // Neither val is NaN, thisVal is smaller
	        if (o.getInd() > this.ind)
	            return 1; 
	        return 0;
		}
		return cmp;
	}


}
