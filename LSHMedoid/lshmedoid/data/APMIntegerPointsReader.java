package lshmedoid.data;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import lshmedoid.test.LSHTest;

import org.apache.commons.math3.stat.clustering.EuclideanIntegerPoint;

public class APMIntegerPointsReader extends DataReader<EuclideanIntegerPoint> {

	/* Read the next non comment line of a file
     * @param stream  the stream from which to read the line
     * @param jumpEmptyLines  if true, read the next non comment, non blank line of the file
     * @param lineNumber  if non null, keeps the line count of the file on this integer
     */
    public String readNextLine(
        BufferedReader stream,
        boolean        jumpEmptyLines)
        throws IOException {
        String l_nextLine;
        do {
            l_nextLine = stream.readLine();
            if(l_nextLine.charAt(l_nextLine.length()-1)=='\n'){
            	l_nextLine=l_nextLine.substring(0,l_nextLine.length()-1);
            }
        } while (l_nextLine != null && 
                 ( (jumpEmptyLines && l_nextLine.trim().length() == 0) || 
                   (l_nextLine.length() > 0 && l_nextLine.charAt(0) == '#') ) );
        return l_nextLine;
    }
    /* Read the next non comment, non blank, line of a file. 
     * Equivalent to readNextLine with jumpEmptyLines = true */
    public String readNextLine(
        BufferedReader stream)
        throws IOException {
        return readNextLine(stream, true);
    }

	@Override
	public ArrayList<EuclideanIntegerPoint> readSignatures(
	        String inputFileName,
	        ArrayList<Integer> idKeypoints )
	        throws IOException {
	            
	        // Opens input file
	        BufferedReader l_input;
	        try {
	            InputStream l_inputStream = new FileInputStream(inputFileName);
	            l_input = new BufferedReader(new InputStreamReader(l_inputStream));
	        }
	        catch (IOException e) {
	            throw new IOException("error opening input file - " + inputFileName);
	        }
	        
	        // Reads file header
	        int l_nDimensions = 128;
	        int l_nImages; 
	        int l_nPoints;
	        String nextline ="";
	        try {
	        	
	            if (! l_input.readLine().equals("KEYPOINTS SIFT")) {
	            	l_input.close();
	                throw new IllegalArgumentException("bad header KEYPOINTS SIFT in input file - " + inputFileName);
	            }
	            nextline= readNextLine(l_input);
	            l_nImages = (int)Double.parseDouble(nextline);
	            nextline = readNextLine(l_input);
	            l_nPoints = (int)Double.parseDouble(nextline);
	            nextline = readNextLine(l_input);
	            if (! nextline.contains("ID IMAGE ROW COL SCALE ANGLE VECTOR")) {
	            	l_input.close();
	            	throw new IllegalArgumentException("bad header in string"+ nextline +"input file - " + inputFileName);
	            }
	        }
	        catch (IOException e) {
	        	l_input.close();
	            throw new IOException("error reading input file header - " + inputFileName+" read:"+nextline);
	        }
	        catch (NumberFormatException e) {
	        	l_input.close();
	            throw new IllegalArgumentException("bad header in input file numberformat - " + inputFileName+" read:"+nextline);
	        }
	        
	        // Reads data points
	        LSHTest.log("Opening file:"+inputFileName);
	        LSHTest.log("l_nImages",l_nImages);
	        LSHTest.log(" l_nPoints="+l_nPoints);
	        
	        long start = System.currentTimeMillis();
	        

	        ArrayList<EuclideanIntegerPoint>  l_points = new ArrayList<EuclideanIntegerPoint>(l_nPoints);
	        idKeypoints.clear();
	        idKeypoints.ensureCapacity(l_nPoints);
	        try {
	        	
	        	for (int i=0; i<l_nPoints; i++) {
	            	
	                String          l_rawData = l_input.readLine();
	                StringTokenizer l_tokens = new StringTokenizer(l_rawData);
	                
	                
	                int ID = Integer.parseInt(l_tokens.nextToken());
	                idKeypoints.add(ID);
	                @SuppressWarnings("unused")	                
	                String        l_imageName = l_tokens.nextToken();
	                @SuppressWarnings("unused")
	                double x     = Double.parseDouble(l_tokens.nextToken());
	                @SuppressWarnings("unused")
					double y     = Double.parseDouble(l_tokens.nextToken());
	                @SuppressWarnings("unused")    
	                double scale = Double.parseDouble(l_tokens.nextToken());
	                @SuppressWarnings("unused")
	                double angle = Double.parseDouble(l_tokens.nextToken());
	                int signature[] = new int[l_nDimensions];
	                for (int j=0; j<l_nDimensions; j++) {
	                    if (!l_tokens.hasMoreTokens()) {
	                        l_rawData = l_input.readLine();
	                        l_tokens = new StringTokenizer(l_rawData);
	                    }
	                    signature[j] = Integer.parseInt(l_tokens.nextToken());;
	                }
	                
	                EuclideanIntegerPoint point=new EuclideanIntegerPoint(signature);
	                l_points.add(point);
	            }
	            l_input.close();
	        }
	        catch (IOException e) {
	        	
	            throw new IOException("error reading input file - " + inputFileName);
	        }        
	        catch (NumberFormatException e) {
	            throw new IllegalArgumentException("syntax error on input file - " + inputFileName);
	        }
	        catch (NoSuchElementException e) {
	            throw new IllegalArgumentException("syntax error on input file! - " + inputFileName);
	        }
	        LSHTest.logTime("closing file "+inputFileName, start );
	        return l_points;
	}
}
