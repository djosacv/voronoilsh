package lshmedoid.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.stat.clustering.Clusterable;

public abstract class DataReader <T extends Clusterable<T>> {

	/**
	 * Implements a dataset reader that loads a dataset located in a file and returns an ArrayList of objets of type <T> in dataset
	 * the arraylist idDataSetList is populated with ids corresponding to each position of the return ArrayList
	 * @param datasetFileName
	 * @param idDataSetList
	 * @return
	 * @throws IOException 
	 */
	public abstract ArrayList<T> readSignatures(String datasetFileName,ArrayList<Integer> idDataSetList) throws IOException;
	
	/**
	 * Implements a dataset reader that loads a dataset located in a list of files and returns an ArrayList of objects of type <T> in dataset
	 * the arraylist idDataSetList is populated with ids corresponding to each position of the return ArrayList
	 * @param datasetFileNames
	 * @param idDataSetList
	 * @return
	 * @throws IOException 
	 */
	public ArrayList<T> readSignatures(List<String> datasetFileNames,ArrayList<Integer> idDataSetList) throws IOException{
		ArrayList<Integer> idlistemp=new ArrayList<Integer>();
		ArrayList<T> datalist=new ArrayList<T>();
		
		idDataSetList.clear();
		for(String filename:datasetFileNames){
			idlistemp.clear();
			datalist.addAll(readSignatures(filename,idlistemp));
			idDataSetList.addAll(idlistemp);
		}
		return datalist;
	}

}
