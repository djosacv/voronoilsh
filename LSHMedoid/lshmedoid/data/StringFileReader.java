/**
 * 
 */
package lshmedoid.data;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import lshmedoid.test.LSHTest;

/**
 * @author eliezer
 *
 */
public class StringFileReader extends DataReader<StringLevenshteinPoint> {

	public String readNextLine(
	        BufferedReader stream,
	        boolean        jumpEmptyLines)
	        throws IOException {
	        String l_nextLine;
	        do {
	            l_nextLine = stream.readLine();
	            if(l_nextLine.charAt(l_nextLine.length()-1)=='\n'){
	            	l_nextLine=l_nextLine.substring(0,l_nextLine.length()-1);
	            }
	        } while (l_nextLine != null && 
	                 ( jumpEmptyLines && l_nextLine.trim().length() == 0)   );
	        return l_nextLine;
	    }
	    /* Read the next non comment, non blank, line of a file. 
	     * Equivalent to readNextLine with jumpEmptyLines = true */
	    public String readNextLine(
	        BufferedReader stream)
	        throws IOException {
	        return readNextLine(stream, true);
	    }
	@Override
	public ArrayList<StringLevenshteinPoint> readSignatures(String datasetFileName,
			ArrayList<Integer> idDataSetList) throws IOException {
		ArrayList<StringLevenshteinPoint> datalist=new ArrayList<StringLevenshteinPoint>();
		BufferedReader l_input;
        try {
            InputStream l_inputStream = new FileInputStream(datasetFileName);
            l_input = new BufferedReader(new InputStreamReader(l_inputStream));
        }
        catch (IOException e) {
            throw new IOException("error opening input file - " + datasetFileName);
        }
        LSHTest.log("Opening file:"+datasetFileName);
        long start = System.currentTimeMillis();

        String stringdata;
        idDataSetList.clear();
        int iddata=1;
        do{
        	stringdata=readNextLine(l_input);
        	datalist.add(new StringLevenshteinPoint(stringdata));
        	idDataSetList.add(new Integer(iddata));
        	iddata++;
        }
        while(stringdata!=null);
        LSHTest.log("Number of data points: "+datalist.size());
        LSHTest.logTime("closing file "+datasetFileName, start );


        
        return datalist;
	}

}
