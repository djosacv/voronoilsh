package lshmedoid.data;

import java.util.Collection;

import org.apache.commons.math3.stat.clustering.Clusterable;

public class StringLevenshteinPoint implements
		Clusterable<StringLevenshteinPoint> {
	private String data;
	
	public StringLevenshteinPoint(String data) {
		this.data = data;
	}
	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	

	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

	@Override
	public double distanceFrom(StringLevenshteinPoint p) {
		// TODO Auto-generated method stub
		return computeLevenshteinDistance(this.getData(),p.getData());
	}

	@Override
	public StringLevenshteinPoint centroidOf(
			Collection<StringLevenshteinPoint> p) {
		// not valid for metric data
		return this;
	}
	
	private static int minimum(int a, int b, int c) {
        return Math.min(Math.min(a, b), c);
	}

	
	public static int computeLevenshteinDistance(CharSequence str1,
                CharSequence str2) {
        int[][] distance = new int[str1.length() + 1][str2.length() + 1];

        for (int i = 0; i <= str1.length(); i++)
                distance[i][0] = i;
        for (int j = 1; j <= str2.length(); j++)
                distance[0][j] = j;

        for (int i = 1; i <= str1.length(); i++)
                for (int j = 1; j <= str2.length(); j++)
                        distance[i][j] = minimum(
                                        distance[i - 1][j] + 1,
                                        distance[i][j - 1] + 1,
                                        distance[i - 1][j - 1]
                                                        + ((str1.charAt(i - 1) == str2.charAt(j - 1)) ? 0
                                                                        : 1));

        return distance[str1.length()][str2.length()];
	}

}
